unit tb_usb1_0_base;

{
  File: libusb1_0.pas
  CreateDate: 01.03.2019
  ModifyDate: 14.11.2024
  Version: 0.2.0.3
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  For more info to LibUsb see https://libusb.info/
}

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  tb_usb_defs,
  tb_libusb1_0,
  tb_libusb1_0_thread;

type
  EbUsb1_0 = class(Exception);

  TbLangId = word;
  TbLangIds = array of TbLangId;

  { IbUsb1_0Base }

  IbUsb1_0Base = interface
    procedure LibInit;
    procedure LibExit;
    procedure SetDebug(Level: Tlibusb_log_level);
    function HasCapability(Capability: Tlibusb_capability): Boolean;
    function ErrorName(ErrorNumber: Integer): AnsiString;
    function StError(ErrorCode: Tlibusb_error): AnsiString;
    procedure GetVersion(out Version: Tlibusb_version);
    function GetHeaderVersion: String;
    function VersionToString(const Version: Tlibusb_version): String;
    procedure SetLocale(const Locale: AnsiString);
    function GetDevice(Index: Integer; DeviceList: PPlibusb_device): Plibusb_device;
    function GetDeviceList(out List: PPlibusb_device): Integer;
    procedure FreeDeviceList(List: PPlibusb_device; unref_devices: Integer);
    function GetBusNumber(Device: Plibusb_device): Byte;
    function GetPortNumber(Device: Plibusb_device): Byte;
    function GetPortNumbers(Device: Plibusb_device; var PortNumbers: array of Byte;
        PortNumbersLen: Integer): Integer;
    function GetParent(Device: Plibusb_device): Plibusb_device;
    function GetDeviceAddress(Device: Plibusb_device): Byte;
    function GetDeviceSpeed(Device: Plibusb_device): Tlibusb_speed;
    function SpeedToString(Speed: Tlibusb_speed): String;
    function GetMaxIsoPacketSize(Device: Plibusb_device;
        Endpoint: Byte): Integer;
    function GetMaxPacketSize(Device: Plibusb_device; Endpoint: Byte): Integer;
    function GetTransferType(Endpoint: Plibusb_endpoint_descriptor): Tlibusb_transfer_type;
    function GetTransferDirection(Endpoint: Plibusb_endpoint_descriptor): Tlibusb_endpoint_direction;
    procedure RefDevice(Device: Plibusb_device);
    procedure UnrefDevice(Device: Plibusb_device);
    procedure OpenDevice(Device: Plibusb_device; out DevHandle: Plibusb_device_handle); overload;
    procedure OpenDevice(VendorId, ProductId: Word;
        out DevHandle: Plibusb_device_handle); overload;
    procedure OpenDevice(VendorId, ProductId: Word; const SerialNumber: String;
        out DevHandle: Plibusb_device_handle); overload;
    procedure CloseDevice(var DevHandle: Plibusb_device_handle);
    function GetDevice(DevHandle: Plibusb_device_handle): Plibusb_device;
    procedure GetConfiguration(DevHandle: Plibusb_device_handle;
        out Config: Integer);
    procedure SetConfiguration(DevHandle: Plibusb_device_handle;
        Config: Integer);
    procedure GetInterfaceDescriptorByIndex(
        ConfigDescriptor: Plibusb_config_descriptor;
        out InterfaceDescriptor: Plibusb_interface_descriptor;
        Index: Integer; AltIndex: Integer=0);
    procedure ClaimInterface(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure ReleaseInterface(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure SetInterfaceAltSetting(DevHandle: Plibusb_device_handle;
        InterfaceNumber, AlternateSetting: Integer);
    procedure ClearHalt(DevHandle: Plibusb_device_handle; Endpoint: Byte);
    procedure ResetDevice(DevHandle: Plibusb_device_handle);
    function KernelDriverActive(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer):Boolean;
    procedure DetachKernelDriver(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure AttachKernelDriver(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure SetAutoDetachKernelDriver(DevHandle: Plibusb_device_handle;
        Enable: Boolean);
    procedure GetDeviceDescriptor(Device: Plibusb_device;
        out desc: Tlibusb_device_descriptor);
    procedure GetActiveConfigDescriptor(Device: Plibusb_device;
        out config: Plibusb_config_descriptor);
    procedure GetConfigDescriptor(Device: Plibusb_device; ConfigIndex: Byte;
        out config: Plibusb_config_descriptor);
    procedure GetConfigDescriptorByValue(Device: Plibusb_device;
        ConfigurationValue: Byte; out config: Plibusb_config_descriptor);
    procedure FreeConfigDescriptor(var config: Plibusb_config_descriptor);
    procedure Get_SS_EndpointCompanionDescriptor(
        Endpoint: Plibusb_endpoint_descriptor;
        out EpComp: Plibusb_ss_endpoint_companion_descriptor);
    procedure Free_SS_EndpointCompanionDescriptor(
        var EpComp: Plibusb_ss_endpoint_companion_descriptor);
    procedure GetBosDescriptor(DevHandle: Plibusb_device_handle;
        out bos: Plibusb_bos_descriptor);
    procedure FreeBosDescriptor(var Bos: Plibusb_bos_descriptor);
    procedure GetUsb_2_0_extension_descriptor(
        DevCap: Plibusb_bos_dev_capability_descriptor;
        out Usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor);
    procedure FreeUsb_2_0_extension_descriptor(
        var Usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor);
    procedure Get_SS_UsbDeviceCapabilityDescriptor(
        DevCap: Plibusb_bos_dev_capability_descriptor;
        out SS_UsbDeviceCap: Plibusb_ss_usb_device_capability_descriptor);
    procedure Free_SS_UsbDeviceCapabilityDescriptor(
        var SS_UsbDeviceCap: Plibusb_ss_usb_device_capability_descriptor);
    procedure GetContainerIdDescriptor(
        DevCap: Plibusb_bos_dev_capability_descriptor;
        out ContainerId: Plibusb_container_id_descriptor);
    procedure FreeContainerIdDescriptor(
        var ContainerId: Plibusb_container_id_descriptor);
    function GetStringDescriptorAscII(DevHandle: Plibusb_device_handle;
      DescIndex: Byte; LangId: TbLangId): AnsiString;
    function GetDescriptor(DevHandle: Plibusb_device_handle;
        DescType, DescIndex:Byte; out Data: array of Byte): Integer;
    function GetStringDescriptor(DevHandle: Plibusb_device_handle;
        DescIndex: Byte; LangId: TbLangId): WideString;
    function GetStringDescriptorAsString(DevHandle: Plibusb_device_handle;
        DescIndex: Byte; LangId: TbLangId): String;
    procedure GetStringLangId(DevHandle: Plibusb_device_handle;
        out LangIds: TbLangIds);
    procedure HotplugRegisterCallback(events: Tlibusb_hotplug_events;
    	  flags: Tlibusb_hotplug_flag; vendor_id: Integer; product_id: Integer;
        dev_class: Integer; cb_fn: Tlibusb_hotplug_callback_fn;
        user_data: Pointer; var callback_handle: Tlibusb_hotplug_callback_handle);
    procedure HotplugDeregisterCallback(
        var callback_handle: Tlibusb_hotplug_callback_handle);
    procedure ControlTransfer(DevHandle: Plibusb_device_handle;
        RequestType: Byte; Request: Tlibusb_standard_request;
        Value: Word; Index: Word; Data: PByte;
        DataLength: Word; Timeout: LongWord);
    procedure BulkTransfer(DevHandle: Plibusb_device_handle;
        Endpoint: Byte; Data: PByte; DataLength: Integer;
        out Transferred: Integer; Timeout: LongWord;
        IgnoreTimeout: Boolean = False);
    procedure InterruptTransfer(DevHandle: Plibusb_device_handle;
        Endpoint: Byte; Data: PByte; DataLength: Integer;
        out Transferred: Integer; Timeout: LongWord;
        IgnoreTimeout: Boolean = False);
    procedure FillControlSetup(buffer: PByte; bmRequestType: Byte;
        bRequest: Byte; wValue: Word; wIndex: Word; wLength: Word);
    procedure FillControlTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; buffer: PByte;
        callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
    procedure FillBulkTransfer(transfer: Plibusb_transfer; dev_handle:
        Plibusb_device_handle; endpoint: Byte; buffer: PByte; length: Integer;
        callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
    procedure FillBulkStreamTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; endpoint: Byte; stream_id: LongWord;
        buffer: PByte; length: Integer; callback: Tlibusb_transfer_cb_fn;
        user_data: Pointer; timeout: LongWord);
    procedure FillInterruptTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte;
        length: Integer; callback: Tlibusb_transfer_cb_fn;
        user_data: Pointer; timeout: LongWord);
    procedure FillIsoTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; endpoint: Byte; buffer:
        PByte; length: Integer; num_iso_packets: Integer;
        callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
    procedure SetIsoPacketLengths(transfer: Plibusb_transfer; length: LongWord);
    procedure SubmitTransfer(transfer: Plibusb_transfer);
    procedure CancelTransfer(transfer: Plibusb_transfer);
    procedure HandleEvents;
    procedure HandleEventsTimeoutCompleted(tv: Plibusb_timeval; completed: PInteger);
    procedure HandleEventsCompleted(completed: PInteger);
    function AllocTransfer(iso_packets: Integer): Plibusb_transfer;
    procedure FreeTransfer(var transfer: Plibusb_transfer);
    function GetAvailableSerialNumbers(VendorId, ProductId: Word): TStringArray;
  end;


  TbUsb_SyncIo =  procedure(DevHandle: Plibusb_device_handle;
        Endpoint: Byte; Data: PByte; DataLength: Integer;
        out Transferred: Integer; Timeout: LongWord;
        IgnoreTimeout: Boolean) of Object;

  { TIbUsb1_0Base }

  TIbUsb1_0Base = class(TUsbInterfacedObject, IbUsb1_0Base)
  private
    ctx: Plibusb_context;
    function CtxGet: Plibusb_context;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LibInit;
    procedure LibExit;
    procedure SetDebug(Level: Tlibusb_log_level);
    function HasCapability(Capability: Tlibusb_capability): Boolean;
    function ErrorName(ErrorCode: Integer): AnsiString;
    function StError(ErrorCode: Tlibusb_error): AnsiString; overload;
    function StError(ErrorCode: Integer): AnsiString; overload;
    procedure GetVersion(out Version: Tlibusb_version);
    function GetHeaderVersion: String;
    function VersionToString(const Version: Tlibusb_version): String;
    procedure SetLocale(const Locale: AnsiString);
    function GetDevice(Index: Integer; DeviceList: PPlibusb_device): Plibusb_device;
    function GetDeviceList(out DeviceList: PPlibusb_device): Integer;
    procedure FreeDeviceList(DeviceList: PPlibusb_device; unref_devices: Integer);
    function GetBusNumber(Device: Plibusb_device): Byte;
    function GetPortNumber(Device: Plibusb_device): Byte;
    function GetPortNumbers(Device: Plibusb_device; var PortNumbers: array of Byte;
        PortNumbersLen: Integer): Integer;
    function GetParent(Device: Plibusb_device): Plibusb_device;
    function GetDeviceAddress(Device: Plibusb_device): Byte;
    function GetDeviceSpeed(Device: Plibusb_device): Tlibusb_speed;
    function SpeedToString(Speed: Tlibusb_speed): String;
    function GetMaxIsoPacketSize(Device: Plibusb_device;
        Endpoint: Byte): Integer;
    function GetMaxPacketSize(Device: Plibusb_device; Endpoint: Byte): Integer;
    function GetTransferType(Endpoint: Plibusb_endpoint_descriptor): Tlibusb_transfer_type;
    function GetTransferDirection(Endpoint: Plibusb_endpoint_descriptor): Tlibusb_endpoint_direction;
    procedure RefDevice(Device: Plibusb_device);
    procedure UnrefDevice(Device: Plibusb_device);
    procedure OpenDevice(Device: Plibusb_device;
        out DevHandle: Plibusb_device_handle); overload;
    procedure OpenDevice(VendorId, ProductId: Word;
        out DevHandle: Plibusb_device_handle); overload;
    procedure OpenDevice(VendorId, ProductId: Word; const SerialNumber: String;
        out DevHandle: Plibusb_device_handle); overload;
    procedure CloseDevice(var DevHandle: Plibusb_device_handle);
    function GetDevice(DevHandle: Plibusb_device_handle): Plibusb_device;
    procedure GetConfiguration(DevHandle: Plibusb_device_handle;
        out Config: Integer);
    procedure SetConfiguration(DevHandle: Plibusb_device_handle;
        Config: Integer);
    procedure GetInterfaceDescriptorByIndex(
        ConfigDescriptor: Plibusb_config_descriptor;
        out InterfaceDescriptor: Plibusb_interface_descriptor;
        Index: Integer; AltIndex: Integer=0);
    procedure ClaimInterface(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure ReleaseInterface(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure SetInterfaceAltSetting(DevHandle: Plibusb_device_handle;
        InterfaceNumber, AlternateSetting: Integer);
    procedure ClearHalt(DevHandle: Plibusb_device_handle; Endpoint: Byte);
    procedure ResetDevice(DevHandle: Plibusb_device_handle);
    function KernelDriverActive(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer):Boolean;
    procedure DetachKernelDriver(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure AttachKernelDriver(DevHandle: Plibusb_device_handle;
        InterfaceNumber: Integer);
    procedure SetAutoDetachKernelDriver(DevHandle: Plibusb_device_handle;
        Enable: Boolean);
    procedure GetDeviceDescriptor(Device: Plibusb_device;
        out Desc: Tlibusb_device_descriptor);
    procedure GetActiveConfigDescriptor(Device: Plibusb_device;
        out Config: Plibusb_config_descriptor);
    procedure GetConfigDescriptor(Device: Plibusb_device; ConfigIndex: Byte;
        out config: Plibusb_config_descriptor);
    procedure GetConfigDescriptorByValue(Device: Plibusb_device;
        ConfigurationValue: Byte; out config: Plibusb_config_descriptor);
    procedure FreeConfigDescriptor(var config: Plibusb_config_descriptor);
    procedure Get_SS_EndpointCompanionDescriptor(
        Endpoint: Plibusb_endpoint_descriptor;
        out EpComp: Plibusb_ss_endpoint_companion_descriptor);
    procedure Free_SS_EndpointCompanionDescriptor(
        var EpComp: Plibusb_ss_endpoint_companion_descriptor);
    procedure GetBosDescriptor(DevHandle: Plibusb_device_handle;
        out bos: Plibusb_bos_descriptor);
    procedure FreeBosDescriptor(var bos: Plibusb_bos_descriptor);
    procedure GetUsb_2_0_extension_descriptor(
        DevCap: Plibusb_bos_dev_capability_descriptor;
        out Usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor);
    procedure FreeUsb_2_0_extension_descriptor(
        var Usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor);
    procedure Get_SS_UsbDeviceCapabilityDescriptor(
        DevCap: Plibusb_bos_dev_capability_descriptor;
        out SS_UsbDeviceCap: Plibusb_ss_usb_device_capability_descriptor);
    procedure Free_SS_UsbDeviceCapabilityDescriptor(
        var SS_UsbDeviceCap: Plibusb_ss_usb_device_capability_descriptor);
    procedure GetContainerIdDescriptor(
        DevCap: Plibusb_bos_dev_capability_descriptor;
        out ContainerId: Plibusb_container_id_descriptor);
    procedure FreeContainerIdDescriptor(
        var ContainerId: Plibusb_container_id_descriptor);
    function GetStringDescriptorAscII(DevHandle: Plibusb_device_handle;
      DescIndex: Byte; LangId: TbLangId): AnsiString;
    function GetDescriptor(DevHandle: Plibusb_device_handle;
        DescType, DescIndex:Byte; out Data: array of Byte): Integer;
    function GetStringDescriptor(DevHandle: Plibusb_device_handle;
        DescIndex: Byte; LangId: TbLangId): WideString;
    function GetStringDescriptorAsString(DevHandle: Plibusb_device_handle;
        DescIndex: Byte; LangId: TbLangId): String;
    procedure GetStringLangId(DevHandle: Plibusb_device_handle;
        out LangIds: TbLangIds);
    procedure HotplugRegisterCallback(events: Tlibusb_hotplug_events;
    	  flags: Tlibusb_hotplug_flag; vendor_id: Integer; product_id: Integer;
        dev_class: Integer; cb_fn: Tlibusb_hotplug_callback_fn;
        user_data: Pointer; var callback_handle: Tlibusb_hotplug_callback_handle);
    procedure HotplugDeregisterCallback(
        var callback_handle: Tlibusb_hotplug_callback_handle);
    procedure ControlTransfer(DevHandle: Plibusb_device_handle;
        RequestType: Byte; Request: Tlibusb_standard_request;
        Value: Word; Index: Word; Data: PByte; DataLength: Word; Timeout: LongWord);
    procedure BulkTransfer(DevHandle: Plibusb_device_handle;
        Endpoint: Byte; Data: PByte; DataLength: Integer;
        out Transferred: Integer; Timeout: LongWord;
        IgnoreTimeout: Boolean = False);
    procedure InterruptTransfer(DevHandle: Plibusb_device_handle;
        Endpoint: Byte; Data: PByte; DataLength: Integer;
        out Transferred: Integer; Timeout: LongWord;
        IgnoreTimeout: Boolean = False);
    procedure FillControlSetup(buffer: PByte; bmRequestType: Byte;
        bRequest: Byte; wValue: Word; wIndex: Word; wLength: Word);
    procedure FillControlTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; buffer: PByte;
        callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
    procedure FillBulkTransfer(transfer: Plibusb_transfer; dev_handle:
        Plibusb_device_handle; endpoint: Byte; buffer: PByte; length: Integer;
        callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
    procedure FillBulkStreamTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; endpoint: Byte; stream_id: LongWord;
        buffer: PByte; length: Integer; callback: Tlibusb_transfer_cb_fn;
        user_data: Pointer; timeout: LongWord);
    procedure FillInterruptTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte;
        length: Integer; callback: Tlibusb_transfer_cb_fn;
        user_data: Pointer; timeout: LongWord);
    procedure FillIsoTransfer(transfer: Plibusb_transfer;
        dev_handle: Plibusb_device_handle; endpoint: Byte; buffer:
        PByte; length: Integer; num_iso_packets: Integer;
        callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
    procedure SetIsoPacketLengths(transfer: Plibusb_transfer; length: LongWord);

    procedure SubmitTransfer(transfer: Plibusb_transfer);
    procedure CancelTransfer(transfer: Plibusb_transfer);
    procedure HandleEvents;
    procedure HandleEventsTimeoutCompleted(tv: Plibusb_timeval; completed: PInteger);
    procedure HandleEventsCompleted(completed: PInteger);

    function AllocTransfer(iso_packets: Integer): Plibusb_transfer;
    procedure FreeTransfer(var transfer: Plibusb_transfer);
    function GetAvailableSerialNumbers(VendorId, ProductId: Word): TStringArray;
  end;

  { TUsb1_0EventThread }

  TUsb1_0EventThread = class(TbUsb1_0Thread)
  private
    iusb: IbUsb1_0Base;
    CompletedTimeout: Tlibusb_timeval;
  protected
    procedure Run; override;
  public
    constructor Create(Usb: IbUsb1_0Base);
    destructor Destroy; override;
  end;



implementation


type
  TbUsbLangIds  = packed record
    bLength: Byte;
    bDescriptorType: Byte;
    wLangIds: array [0..125] of TbLangId;
  end;

  TbUsbStringDesc = packed record
    bLength: Byte;
    bDescriptorType: Byte;
    uString: array [0..125] of WideChar;
  end;


function WideStringLength(UsbDescString: TbUsbStringDesc): Integer;
begin
  for Result:= 0 to High(UsbDescString.uString) do begin
    if Word(UsbDescString.uString[Result]) = 0 then exit;
  end;
  Result:= 0;
end;

{ TUsb1_0EventThread }

procedure TUsb1_0EventThread.Run;
var
  Completed: Integer;
begin
{ TODO -otb : Über das fangen von Exception nach denken. }
  while not Finalized do begin
    iusb.HandleEventsTimeoutCompleted(@CompletedTimeout, @Completed);
  end;
end;

constructor TUsb1_0EventThread.Create(Usb: IbUsb1_0Base);
begin
  iusb:= Usb;
  CompletedTimeout.tv_sec:= 1;
  CompletedTimeout.tv_usec:= 0;
  inherited Create(False);
end;

destructor TUsb1_0EventThread.Destroy;
begin
  Finalize;	  //Das Beenden des Threads einleiten
//  iusb.HandleEventsCompleted(nil);   tut es nicht
  WaitFor;    //Warten bis der Thread beendet ist
  inherited;
end;


{ TIbUsb1_0Base }

function TIbUsb1_0Base.CtxGet: Plibusb_context;
begin
  if not Assigned(ctx) then
    raise EbUsb1_0.Create('TbUsb1_0Base.CtxGet:: Context (ctx), not assigned.');
  Result:= ctx;
end;

function TIbUsb1_0Base.GetDevice(Index: Integer; DeviceList: PPlibusb_device
  ): Plibusb_device;
begin
  Result:= DeviceList[Index];
end;

constructor TIbUsb1_0Base.Create;
begin
  inherited;
  LibusbLibaryLoad;
  LibInit;
end;

destructor TIbUsb1_0Base.Destroy;
begin
  LibExit;
  LibusbLibaryFree;
  inherited Destroy;
end;

procedure TIbUsb1_0Base.LibInit;
begin
  try
     libusb_lock;
     if not Assigned(ctx) then begin
       if libusb_init(ctx) <> Integer(LIBUSB_SUCCESS) then begin
         raise EbUsb1_0.Create('TbUsb1_0Base.Init:: Lib USB not initialized.');
       end;
     end;
  finally
    libusb_unlock;
  end;
end;

procedure TIbUsb1_0Base.LibExit;
begin
  try
     libusb_lock;
     if Assigned(ctx) then begin
       libusb_exit(ctx);
       ctx:= nil;
     end;
  finally
    libusb_unlock;
  end;
end;

procedure TIbUsb1_0Base.SetDebug(Level: Tlibusb_log_level);
begin
  libusb_set_debug(CtxGet, Integer(Level));
end;

function TIbUsb1_0Base.HasCapability(Capability: Tlibusb_capability): Boolean;
begin
  Result:= libusb_has_capability(UInt32(Capability)) <> 0;
end;

function TIbUsb1_0Base.ErrorName(ErrorCode: Integer): AnsiString;
begin
  Result:= AnsiString(libusb_error_name(ErrorCode));
end;

function TIbUsb1_0Base.StError(ErrorCode: Tlibusb_error): AnsiString;
begin
  Result:= AnsiString(libusb_strerror(ErrorCode));
end;

function TIbUsb1_0Base.StError(ErrorCode: Integer): AnsiString;
begin
  Result:= StError(Tlibusb_error(ErrorCode));
end;

procedure TIbUsb1_0Base.GetVersion(out Version: Tlibusb_version);
var
  v: Plibusb_version;
begin
  v:= libusb_get_version();
{$PUSH}{$WARN 5058 OFF}
  move(v^, Version, SizeOf(Version));
{$POP}
end;

function TIbUsb1_0Base.GetHeaderVersion: String;
begin
  Result:= CLIBUSB_ORIGINAL_H_VERSION;
end;

function TIbUsb1_0Base.VersionToString(const Version: Tlibusb_version): String;
begin
  Result:= Format('%d.%d.%d.%d',
           [Version.major, Version.minor, Version.micro, Version.nano]);
end;

procedure TIbUsb1_0Base.SetLocale(const Locale: AnsiString);
begin
  if libusb_setlocale(PAnsiChar(Locale)) <> Integer(LIBUSB_SUCCESS) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.SetLocale:: Not seted.');
  end;
end;

function TIbUsb1_0Base.GetDeviceList(out DeviceList: PPlibusb_device): Integer;
begin
  Result:= libusb_get_device_list(ctx, DeviceList);
  if Result < 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetDeviceList:: Error=%s', [StError(Result)]);
  end;
end;

procedure TIbUsb1_0Base.FreeDeviceList(DeviceList: PPlibusb_device;
  unref_devices: Integer);
begin
  libusb_free_device_list(DeviceList, unref_devices);
end;

function TIbUsb1_0Base.GetBusNumber(Device: Plibusb_device): Byte;
begin
  Result:= libusb_get_bus_number(Device);
end;

function TIbUsb1_0Base.GetPortNumber(Device: Plibusb_device): Byte;
begin
  Result:= libusb_get_port_number(Device);
end;

function TIbUsb1_0Base.GetPortNumbers(Device: Plibusb_device;
         var PortNumbers: array of Byte; PortNumbersLen: Integer): Integer;
begin
  Result:= libusb_get_port_numbers(Device, PortNumbers, PortNumbersLen);
  if Result < 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetPortNumbers:: Error=%s', [StError(Result)]);
  end;
end;

function TIbUsb1_0Base.GetParent(Device: Plibusb_device): Plibusb_device;
begin
  Result:= libusb_get_parent(Device);
end;

function TIbUsb1_0Base.GetDeviceAddress(Device: Plibusb_device): Byte;
begin
  Result:= libusb_get_device_address(Device);
end;

function TIbUsb1_0Base.GetDeviceSpeed(Device: Plibusb_device): Tlibusb_speed;
begin
  Result:= Tlibusb_speed(libusb_get_device_speed(Device));
end;

function TIbUsb1_0Base.SpeedToString(Speed: Tlibusb_speed): String;
const
  C_SPEED_STRINGS: array [Tlibusb_speed] of String = (
    'The OS doesn''t report or know the device speed',
    'Low speed (1.5MBit/s)',
    'Full speed (12MBit/s)',
    'High speed (480MBit/s)',
    'Super speed (5000MBit/s)',
    'Super speed plus (10000MBit/s)'
  );
begin
  Result:= C_SPEED_STRINGS[Speed];
end;

function TIbUsb1_0Base.GetMaxPacketSize(Device: Plibusb_device; Endpoint: Byte): Integer;
begin
  Result:= libusb_get_max_packet_size(Device, Endpoint);
  if Result < 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetMaxPacketSize:: Error=%s', [StError(Result)]);
  end;
end;

function TIbUsb1_0Base.GetTransferType(Endpoint: Plibusb_endpoint_descriptor): Tlibusb_transfer_type;
begin
  if not Assigned(Endpoint) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetTranferType:: Endpoint not assigned.');
  end;
  Result:= Tlibusb_transfer_type(LIBUSB_TRANSFER_TYPE_MASK and Endpoint^.bmAttributes);
end;

function TIbUsb1_0Base.GetTransferDirection(Endpoint: Plibusb_endpoint_descriptor): Tlibusb_endpoint_direction;
begin
if not Assigned(Endpoint) then begin
  raise EbUsb1_0.Create('TbUsb1_0Base.GetTransferDirection:: Endpoint not assigned.');
end;
  Result:= Tlibusb_endpoint_direction(Byte(LIBUSB_ENDPOINT_IN) and Endpoint^.bEndpointAddress);
end;

function TIbUsb1_0Base.GetMaxIsoPacketSize(Device: Plibusb_device; Endpoint: Byte): Integer;
begin
  Result:= libusb_get_max_iso_packet_size(Device, Endpoint);
  if Result < 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetMaxPacketSize:: Error=%s', [StError(Result)]);
  end;
end;

procedure TIbUsb1_0Base.RefDevice(Device: Plibusb_device);
begin
  libusb_ref_device(Device);
end;

procedure TIbUsb1_0Base.UnrefDevice(Device: Plibusb_device);
begin
  libusb_unref_device(Device);
end;

procedure TIbUsb1_0Base.OpenDevice(Device: Plibusb_device;
         out DevHandle: Plibusb_device_handle);
var
  retval: Integer;
begin
  retval:= libusb_open(Device, DevHandle);
  if Tlibusb_error(retval) <> LIBUSB_SUCCESS then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.OpenDevice:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.OpenDevice(VendorId, ProductId: Word;
         out DevHandle: Plibusb_device_handle);
begin
  DevHandle:= libusb_open_device_with_vid_pid(CtxGet, VendorId, ProductId);
  if not Assigned(DevHandle) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.Open (VID, PID):: Device not open.');
  end;
end;

{$PUSH}{$WARN 5057 OFF}
procedure TIbUsb1_0Base.OpenDevice(VendorId, ProductId: Word;
  const SerialNumber: String; out DevHandle: Plibusb_device_handle);
var
  number_devices, i: Integer;
  device: Plibusb_device;
  devices_list: PPlibusb_device;
  device_desc: Tlibusb_device_descriptor;
  device_handle: Plibusb_device_handle;
  lang_ids: TbLangIds;
  sn: String;
begin
  try
    number_devices:= GetDeviceList(devices_list);
    for i:= 0 to number_devices -1 do begin
      device:= GetDevice(i, devices_list);
      GetDeviceDescriptor(device, device_desc);
      if (device_desc.idVendor = VendorId)
      and (device_desc.idProduct = ProductId) then begin
        OpenDevice(device, device_handle);
        if SerialNumber = '' then begin
          DevHandle:= device_handle;
          exit;
        end;
{$PUSH}{$WARN 5091 OFF}
        GetStringLangId(device_handle, lang_ids);
{$POP}
        sn:= GetStringDescriptorAsString(device_handle, device_desc.iSerialNumber, lang_ids[0]);
        if sn = SerialNumber then begin
          DevHandle:= device_handle;
          exit;
        end else begin
          CloseDevice(device_handle);
        end;
      end;
    end;
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.OpenDevice:: Device not available, VID=%4x, PID=%4x, SN=%s' ,
        [VendorId, ProductId, SerialNumber]);
  finally
    FreeDeviceList(devices_list, number_devices);
  end;
end;
{$POP}

procedure TIbUsb1_0Base.CloseDevice(var DevHandle: Plibusb_device_handle);
begin
  if Assigned(DevHandle) then begin
    libusb_close(DevHandle);
    DevHandle:= nil;
  end;
end;

function TIbUsb1_0Base.GetDevice(DevHandle: Plibusb_device_handle): Plibusb_device;
begin
  if Assigned(DevHandle) then begin
    Result:= libusb_get_device(DevHandle);
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetDevice:: DevHandle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.GetConfiguration(DevHandle: Plibusb_device_handle;
  out Config: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_get_configuration(DevHandle, Config);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetConfiguration:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetConfiguration:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.SetConfiguration(DevHandle: Plibusb_device_handle;
  Config: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_set_configuration(DevHandle, Config);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.SetConfiguration:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.SetConfiguration:: dev_handle not assigned.');
  end;
end;

// Helper function to get a interface from the config descriptor
// Not include in libusb
procedure TIbUsb1_0Base.GetInterfaceDescriptorByIndex(
    ConfigDescriptor: Plibusb_config_descriptor;
    out InterfaceDescriptor: Plibusb_interface_descriptor;
    Index: Integer; AltIndex: Integer=0);
begin
  if Assigned(ConfigDescriptor) then begin
    if (Index <= ConfigDescriptor^.bNumInterfaces) and
       (AltIndex < ConfigDescriptor^.usb_interface[Index].num_altsetting ) then begin
      InterfaceDescriptor:= @ConfigDescriptor^.usb_interface[Index].altsetting[AltIndex];
    end else begin
      raise EbUsb1_0.Create('TbUsb1_0Base.GetInterfaceDescriptorByIndex:: Index not available.');
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetInterfaceDescriptorByIndex:: ConfigDescriptor not assigned.');
  end;
end;


procedure TIbUsb1_0Base.ClaimInterface(DevHandle: Plibusb_device_handle;
  InterfaceNumber: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_claim_interface(DevHandle, InterfaceNumber);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.ClaimInterface:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.ClaimInterface:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.ReleaseInterface(DevHandle: Plibusb_device_handle;
  InterfaceNumber: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_release_interface(DevHandle, InterfaceNumber);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.ReleaseInterface:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.ReleaseInterface:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.SetInterfaceAltSetting(
  DevHandle: Plibusb_device_handle; InterfaceNumber,
  AlternateSetting: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_set_interface_alt_setting(DevHandle, InterfaceNumber, AlternateSetting);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.SetInterfaceAltSetting:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.SetInterfaceAltSetting:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.ClearHalt(DevHandle: Plibusb_device_handle; Endpoint: Byte);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_clear_halt(DevHandle, Endpoint);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.ClearHalt:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.ClearHalt:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.ResetDevice(DevHandle: Plibusb_device_handle);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_reset_device(DevHandle);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.ResetDevice:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.ResetDevice:: dev_handle not assigned.');
  end;
end;

function TIbUsb1_0Base.KernelDriverActive(DevHandle: Plibusb_device_handle;
  InterfaceNumber: Integer): Boolean;
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_kernel_driver_active(DevHandle, InterfaceNumber);
    if retval < 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.KernelDriverActive:: Error=%s', [StError(retval)]);
    end;
    Result:= retval = 1;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.KernelDriverActive:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.DetachKernelDriver(DevHandle: Plibusb_device_handle;
  InterfaceNumber: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_detach_kernel_driver(DevHandle, InterfaceNumber);
    if retval < 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.DetachKernelDriver:: Not supported on Darwin and Windows, Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.DetachKernelDriver:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.AttachKernelDriver(DevHandle: Plibusb_device_handle;
  InterfaceNumber: Integer);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_attach_kernel_driver(DevHandle, InterfaceNumber);
    if retval < 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.AttachKernelDriver:: Not supported on Darwin and Windows, Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.AttachKernelDriver:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.SetAutoDetachKernelDriver(
   DevHandle: Plibusb_device_handle; Enable: Boolean);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_set_auto_detach_kernel_driver(DevHandle, Integer(Enable));
    if retval < 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.SetAutoDetachKernelDriver:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.SetAutoDetachKernelDriver:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.GetDeviceDescriptor(Device: Plibusb_device; out
  Desc: Tlibusb_device_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_device_descriptor(Device, Desc);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetDeviceDescriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.GetActiveConfigDescriptor(Device: Plibusb_device;
  out Config: Plibusb_config_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_active_config_descriptor(Device, Config);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetActiveConfigDescriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.GetConfigDescriptor(Device: Plibusb_device;
  ConfigIndex: Byte; out config: Plibusb_config_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_config_descriptor(Device, ConfigIndex, Config);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetConfigDescriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.GetConfigDescriptorByValue(Device: Plibusb_device;
  ConfigurationValue: Byte; out config: Plibusb_config_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_config_descriptor_by_value(Device, ConfigurationValue, Config);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetConfigDescriptorByValue:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.FreeConfigDescriptor(var config: Plibusb_config_descriptor);
begin
  if Assigned(config) then begin
    libusb_free_config_descriptor(config);
    config:= nil;
  end;
end;

procedure TIbUsb1_0Base.Get_SS_EndpointCompanionDescriptor(
  Endpoint: Plibusb_endpoint_descriptor;
  out EpComp: Plibusb_ss_endpoint_companion_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_ss_endpoint_companion_descriptor(CtxGet, Endpoint, EpComp);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.Get_SS_EndpointCompanionDescriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.Free_SS_EndpointCompanionDescriptor(
  var EpComp: Plibusb_ss_endpoint_companion_descriptor);
begin
  if Assigned(EpComp) then begin
    libusb_free_ss_endpoint_companion_descriptor(EpComp);
    EpComp:= nil;
  end;
end;

procedure TIbUsb1_0Base.GetBosDescriptor(DevHandle: Plibusb_device_handle;
  out bos: Plibusb_bos_descriptor);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_get_bos_descriptor(DevHandle, bos);
    if retval <> 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetBosDescriptor:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetBosDescriptor:: dev_handle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.FreeBosDescriptor(var bos: Plibusb_bos_descriptor);
begin
  if Assigned(bos) then begin
    libusb_free_bos_descriptor(bos);
    bos:= nil
  end;
end;

procedure TIbUsb1_0Base.GetUsb_2_0_extension_descriptor(
  DevCap: Plibusb_bos_dev_capability_descriptor;
  out Usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_usb_2_0_extension_descriptor(CtxGet, DevCap, Usb_2_0_extension);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetUsb_2_0_extension_descriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.FreeUsb_2_0_extension_descriptor(
  var Usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor);
begin
  if Assigned(Usb_2_0_extension) then begin
    libusb_free_usb_2_0_extension_descriptor(Usb_2_0_extension);
    Usb_2_0_extension:= nil;
  end;
end;

procedure TIbUsb1_0Base.Get_SS_UsbDeviceCapabilityDescriptor(
  DevCap: Plibusb_bos_dev_capability_descriptor;
  out SS_UsbDeviceCap: Plibusb_ss_usb_device_capability_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_ss_usb_device_capability_descriptor(CtxGet, DevCap, SS_UsbDeviceCap);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.libusb_get_usb_2_0_extension_descriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.Free_SS_UsbDeviceCapabilityDescriptor(
  var SS_UsbDeviceCap: Plibusb_ss_usb_device_capability_descriptor);
begin
  if Assigned(SS_UsbDeviceCap) then begin
    libusb_free_ss_usb_device_capability_descriptor(SS_UsbDeviceCap);
    SS_UsbDeviceCap:= nil;
  end;
end;

procedure TIbUsb1_0Base.GetContainerIdDescriptor(
  DevCap: Plibusb_bos_dev_capability_descriptor;
  out ContainerId: Plibusb_container_id_descriptor);
var
  retval: Integer;
begin
  retval:= libusb_get_container_id_descriptor(CtxGet, DevCap, ContainerId);
  if retval <> 0 then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetContainerIdDescriptor:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.FreeContainerIdDescriptor(
  var ContainerId: Plibusb_container_id_descriptor);
begin
  if Assigned(ContainerId) then begin
    libusb_free_container_id_descriptor(ContainerId);
    ContainerId:= nil;
  end;
end;

function TIbUsb1_0Base.GetStringDescriptorAscII(DevHandle: Plibusb_device_handle;
  DescIndex: Byte; LangId: TbLangId): AnsiString;
begin
  if Assigned(DevHandle) then begin
    Result:= AnsiString(GetStringDescriptor(DevHandle, DescIndex, LangId));
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetStringDescriptorAscII:: dev_handle not assigned.');
  end;
end;

function TIbUsb1_0Base.GetDescriptor(DevHandle: Plibusb_device_handle;
  DescType, DescIndex: Byte; out Data: array of Byte): Integer;
begin
  if Assigned(DevHandle) then begin
    Result:= libusb_get_descriptor(DevHandle, DescType, DescIndex, Data, Length(Data));
    if Result < 0 then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetDescriptor:: Error=%s', [StError(Result)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetDescriptor:: dev_handle not assigned.');
  end;
end;

{$PUSH}{$WARN 5057 OFF}
function TIbUsb1_0Base.GetStringDescriptor(DevHandle: Plibusb_device_handle;
  DescIndex: Byte; LangId: TbLangId): WideString;
var
  data: TbUsbStringDesc;
  retval: Integer;
  string_len, wide_len: Integer;
begin
  if Assigned(DevHandle) then begin
    if DescIndex > 0 then begin
      FillByte(data, SizeOf(data), 0);
      retval:= libusb_get_string_descriptor(DevHandle, DescIndex, LangId, @data, SizeOf(data));
      if retval >= 0 then begin
        if data.bLength > 2 then begin
          wide_len:= WideStringLength(data);
          string_len:= (data.bLength - 2) div 2;
          if wide_len < string_len then begin
            string_len:= wide_len;
          end;
{$PUSH}{$WARN 5094 OFF}
          SetLength(Result, string_len);
{$POP}
          Move(data.uString[0], Result[1], string_len * 2);
        end else begin
          Result:= '';
        end;
      end else begin
        raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetStringDescriptor:: Error=%s', [StError(retval)]);
      end;
    end else begin
      Result:= '';
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetStringDescriptor:: DevHandle not assigned.');
  end;
end;
{$POP}

function TIbUsb1_0Base.GetStringDescriptorAsString(
  DevHandle: Plibusb_device_handle; DescIndex: Byte; LangId: TbLangId): String;
begin
  if Assigned(DevHandle) then begin
    Result:= String(GetStringDescriptor(DevHandle, DescIndex, LangId));
//    Result:= AnsiString(GetStringDescriptor(DevHandle, DescIndex, LangId));
//  Result:= '0.0.0.0.0.0.A';
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetStringDescriptorAsString:: DevHandle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.GetStringLangId(DevHandle: Plibusb_device_handle; out LangIds: TbLangIds);
var
  lang_ids: TbUsbLangIds;
  retval, len, i: Integer;

begin
  if Assigned(DevHandle) then begin
{$PUSH}{$WARN 5092 OFF}
    SetLength(LangIds, 0);
{$POP}
    retval:= libusb_get_string_descriptor(DevHandle, 0, 0, @lang_ids, SizeOf(lang_ids));
    if retval > 0 then begin
      if lang_ids.bLength > 2 then begin
        len:= (lang_ids.bLength - 2) div 2;
        SetLength(LangIds, len);
        for i:= 0 to len - 1 do begin
          LangIds[i]:= libusb_cpu_to_le16(lang_ids.wLangIds[i]);
        end;
      end else begin
        SetLength(LangIds, 0);
      end;
    end else begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.GetStringLangId:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.GetStringLangId:: DevHandle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.HotplugRegisterCallback(
  events: Tlibusb_hotplug_events; flags: Tlibusb_hotplug_flag;
  vendor_id: Integer; product_id: Integer; dev_class: Integer;
  cb_fn: Tlibusb_hotplug_callback_fn; user_data: Pointer;
  var callback_handle: Tlibusb_hotplug_callback_handle);
var
  retval: Integer;
begin
  retval:= libusb_hotplug_register_callback(ctx, events, flags, vendor_id, product_id,
      dev_class, cb_fn, user_data, callback_handle);
  if retval <> Integer(LIBUSB_SUCCESS) then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.libusb_hotplug_register_callback:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.HotplugDeregisterCallback(
  var callback_handle: Tlibusb_hotplug_callback_handle);
begin
  if callback_handle <> 0 then begin
    libusb_hotplug_deregister_callback(ctx, callback_handle);
    callback_handle:= 0;
  end;
end;

procedure TIbUsb1_0Base.ControlTransfer(DevHandle: Plibusb_device_handle;
  RequestType: Byte; Request: Tlibusb_standard_request; Value: Word;
  Index: Word; Data: PByte; DataLength: Word; Timeout: LongWord);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_control_transfer(DevHandle, RequestType, Request, Value,
        Index, Data, DataLength, Timeout);
    if retval <> DataLength then begin
      if retval < 0 then begin
        raise EbUsb1_0.CreateFmt('TbUsb1_0Base.ControlTransfer:: Error=%s', [StError(retval)]);
      end else begin
        raise EbUsb1_0.CreateFmt('TbUsb1_0Base.ControlTransfer:: Not all data transfered, retval=%d', [retval]);
      end;
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.ControlTransfer:: DevHandle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.BulkTransfer(DevHandle: Plibusb_device_handle;
  Endpoint: Byte; Data: PByte; DataLength: Integer; out Transferred: Integer;
  Timeout: LongWord; IgnoreTimeout: Boolean);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_bulk_transfer(DevHandle, Endpoint, Data, DataLength,
        Transferred, Timeout);
    if (retval = Integer(LIBUSB_ERROR_TIMEOUT)) and IgnoreTimeout then exit;
    if retval <> Integer(LIBUSB_SUCCESS) then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.BulkTransfer:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.BulkTransfer:: DevHandle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.InterruptTransfer(DevHandle: Plibusb_device_handle;
  Endpoint: Byte; Data: PByte; DataLength: Integer; out Transferred: Integer;
  Timeout: LongWord; IgnoreTimeout: Boolean);
var
  retval: Integer;
begin
  if Assigned(DevHandle) then begin
    retval:= libusb_interrupt_transfer(DevHandle, Endpoint, Data, DataLength,
        Transferred, Timeout);
//{$Hint Test Timeout ist kein Fehler}
    if (retval = Integer(LIBUSB_ERROR_TIMEOUT)) and IgnoreTimeout then exit;
    if retval <> Integer(LIBUSB_SUCCESS) then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.InterruptTransfer:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.InterruptTransfer:: DevHandle not assigned.');
  end;
end;

procedure TIbUsb1_0Base.FillControlSetup(buffer: PByte; bmRequestType: Byte;
  bRequest: Byte; wValue: Word; wIndex: Word; wLength: Word);
var
  setup: Plibusb_control_setup;
begin
  if not Assigned(buffer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillControlSetup:: buffer not assigned.');
  end;
  setup:= Plibusb_control_setup(buffer);
  setup^.bmRequestType:= bmRequestType;
  setup^.bRequest:= bRequest;
  setup^.wValue:= libusb_cpu_to_le16(wValue);
  setup^.wIndex:= libusb_cpu_to_le16(wIndex);
  setup^.wLength:= libusb_cpu_to_le16(wLength);
end;

procedure TIbUsb1_0Base.FillControlTransfer(transfer: Plibusb_transfer;
  dev_handle: Plibusb_device_handle; buffer: PByte;
  callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
var
  setup: Plibusb_control_setup;
begin
  if not Assigned(transfer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillControlTransfer:: transfer not assigned.');
  end;
  if not Assigned(buffer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillControlTransfer:: buffer not assigned.');
  end;

  setup:= Plibusb_control_setup(buffer);
  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= 0;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_CONTROL);
  transfer^.timeout:= timeout;
  if setup <> nil then begin
    transfer^.length:= LIBUSB_CONTROL_SETUP_SIZE + libusb_le16_to_cpu(setup^.wLength);
  end;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end;

procedure TIbUsb1_0Base.FillBulkTransfer(transfer: Plibusb_transfer;
  dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte;
  length: Integer; callback: Tlibusb_transfer_cb_fn; user_data: Pointer;
  timeout: LongWord);
begin
  if not Assigned(transfer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillBulkTransfer:: transfer not assigned.');
  end;
  if not Assigned(buffer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillBulkTransfer:: buffer not assigned.');
  end;

  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= endpoint;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_BULK);
  transfer^.timeout:= timeout;
  transfer^.buffer:= buffer;
  transfer^.length:= length;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end;

procedure TIbUsb1_0Base.FillBulkStreamTransfer(transfer: Plibusb_transfer;
  dev_handle: Plibusb_device_handle; endpoint: Byte; stream_id: LongWord;
  buffer: PByte; length: Integer; callback: Tlibusb_transfer_cb_fn;
  user_data: Pointer; timeout: LongWord);
begin
  if not Assigned(transfer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillBulkTransfer:: transfer not assigned.');
  end;
  if not Assigned(buffer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillBulkStreamTransfer:: buffer not assigned.');
  end;

  FillBulkTransfer(transfer, dev_handle, endpoint, buffer, length, callback,
    user_data, timeout);
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_BULK_STREAM_30);
  libusb_transfer_set_stream_id(transfer, stream_id);
end;

procedure TIbUsb1_0Base.FillInterruptTransfer(transfer: Plibusb_transfer;
  dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte;
  length: Integer; callback: Tlibusb_transfer_cb_fn; user_data: Pointer;
  timeout: LongWord);
begin
  if not Assigned(transfer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillInterruptTransfer:: transfer not assigned.');
  end;
  if not Assigned(buffer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillInterruptTransfer:: buffer not assigned.');
  end;

  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= endpoint;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_INTERRUPT);
  transfer^.timeout:= timeout;
  transfer^.buffer:= buffer;
  transfer^.length:= length;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end;

{$PUSH}{$WARN 5024 OFF}
procedure TIbUsb1_0Base.FillIsoTransfer(transfer: Plibusb_transfer;
  dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte;
  length: Integer; num_iso_packets: Integer; callback: Tlibusb_transfer_cb_fn;
  user_data: Pointer; timeout: LongWord);
begin
  if not Assigned(transfer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillIsoTransfer:: transfer not assigned.');
  end;
  if not Assigned(buffer) then begin
    raise EbUsb1_0.Create('TbUsb1_0Base.FillIsoTransfer:: buffer not assigned.');
  end;

  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= endpoint;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_ISOCHRONOUS);
  transfer^.timeout:= timeout;
  transfer^.buffer:= buffer;
// Wird in AllocTransfer gesetzt
//	transfer^.num_iso_packets:= num_iso_packets;
  transfer^.length:= length;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end; {$POP}

procedure TIbUsb1_0Base.SetIsoPacketLengths(transfer: Plibusb_transfer;
  length: LongWord);
var
  i: Integer;
begin
  for i:= 0 to transfer^.num_iso_packets - 1 do begin
    transfer^.iso_packet_desc[i].length:= length;
  end;
end;


procedure TIbUsb1_0Base.SubmitTransfer(transfer: Plibusb_transfer);
var
  retval: Integer;
begin
  if Assigned(transfer) then begin
    retval:= libusb_submit_transfer(transfer);
    if retval <> Integer(LIBUSB_SUCCESS) then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.SubmitTransfer:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.SubmitTransfer:: transfer not assigned.');
  end;
end;

procedure TIbUsb1_0Base.CancelTransfer(transfer: Plibusb_transfer);
var
  retval: Integer;
begin
  if Assigned(transfer) then begin
    retval:= libusb_cancel_transfer(transfer);
    if retval <> Integer(LIBUSB_SUCCESS) then begin
      raise EbUsb1_0.CreateFmt('TbUsb1_0Base.CancelTransfer:: Error=%s', [StError(retval)]);
    end;
  end else begin
    raise EbUsb1_0.Create('TbUsb1_0Base.CancelTransfer:: transfer not assigned.');
  end;
end;

procedure TIbUsb1_0Base.HandleEvents;
var
  retval: Integer;
begin
  retval:= libusb_handle_events(ctx);
  if retval <> Integer(LIBUSB_SUCCESS) then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.HandleEvents:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.HandleEventsTimeoutCompleted(tv: Plibusb_timeval;
  completed: PInteger);
var
  retval: Integer;
begin
  retval:= libusb_handle_events_timeout_completed(ctx, tv, completed);
  if retval <> Integer(LIBUSB_SUCCESS) then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.HandleEventsTimeoutCompleted:: Error=%s', [StError(retval)]);
  end;
end;

procedure TIbUsb1_0Base.HandleEventsCompleted(completed: PInteger);
var
  retval: Integer;
begin
  retval:= libusb_handle_events_completed(ctx, completed);
  if retval <> Integer(LIBUSB_SUCCESS) then begin
    raise EbUsb1_0.CreateFmt('TbUsb1_0Base.HandleEventsCompleted:: Error=%s', [StError(retval)]);
  end;
end;

function TIbUsb1_0Base.AllocTransfer(iso_packets: Integer): Plibusb_transfer;
begin
  if iso_packets <= LIBUSB_MAX_ISO_PACKET_DESCRIPTOR then begin;
    Result:= libusb_alloc_transfer(iso_packets);
    Result^.num_iso_packets:= iso_packets;
  end else begin
    raise EbUsb1_0.CreateFmt('AllocTransfer:: To mutch iso_packets, the maximum is $d', [LIBUSB_MAX_ISO_PACKET_DESCRIPTOR]);
  end;
end;

procedure TIbUsb1_0Base.FreeTransfer(var transfer: Plibusb_transfer);
begin
  if Assigned(transfer) then begin
    libusb_free_transfer(transfer);
    transfer:= nil;
  end;
end;

{$PUSH}{$WARN 5057 OFF}
function TIbUsb1_0Base.GetAvailableSerialNumbers(
    VendorId, ProductId: Word): TStringArray;
var
  number_devices, i, l: Integer;
  device: Plibusb_device;
  devices_list: PPlibusb_device;
  device_desc: Tlibusb_device_descriptor;
  device_handle: Plibusb_device_handle;
  lang_ids: TbLangIds;
  sn: String;
begin
{$PUSH}{$WARN 5093 OFF}
  SetLength(Result, 0);
{$POP}
  try
    number_devices:= GetDeviceList(devices_list);
    for i:= 0 to number_devices -1 do begin
      device:= GetDevice(i, devices_list);
      GetDeviceDescriptor(device, device_desc);
      if (device_desc.idVendor = VendorId)
      and (device_desc.idProduct = ProductId) then try
        OpenDevice(device, device_handle);
{$PUSH}{$WARN 5091 OFF}
        GetStringLangId(device_handle, lang_ids);
  {$POP}
        sn:= GetStringDescriptorAsString(device_handle, device_desc.iSerialNumber, lang_ids[0]);
        if sn <> '' then begin
          l:= Length(Result);
          SetLength(Result, l+1);
          Result[l]:= sn
        end
      finally;
        CloseDevice(device_handle);
      end;
    end;
  finally
    FreeDeviceList(devices_list, number_devices);
  end;
end;
{$POP}

end.

