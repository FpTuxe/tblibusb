unit tb_libusb1_0;

{
  File: libusb1_0.pas
  CreateDate: 01.03.2019
  ModifyDate: 28.09.2023
  Version: 0.2.0.0
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  For more info to LibUsb see https://libusb.info/
}

{$mode objfpc}{$H+}

interface

{
  Automatically converted by H2Pas 1.0.0 from libusb.h
  The following command line parameters were used:
    -tp
    libusb.h
}

{$if defined(LINUX)}
uses
//  BaseUnix, //include size_t
  SysUtils,
  unixtype;
{$endif}

{$if defined(Windows)}
uses
//  BaseUnix, //include size_t
  SysUtils;
{$endif}


const
  CLIBUSB_ORIGINAL_H_VERSION = '1.0.22';

{$if defined(LINUX)}
  LIBUSB_1_0_NAME = 'libusb-1.0.so.0';
{$elseif defined(WINDOWS)}
  LIBUSB_1_0_NAME = 'libusb-1.0.dll';
{$else}
  {$ERROR  Platform not defined!}
{$endif}

type
  ELibUsb = class(Exception);

{$if defined(LINUX)}
  Tlibusb_timeval = timeval;
  Plibusb_timeval = ^Tlibusb_timeval;
{$endif}

{$Ifdef WINDOWS}
type
  time_t = Int64;
  timeval = record
    tv_sec: time_t;    //Seconds
    tv_usec: Int64;    //Microseconds
  end;
  Tlibusb_timeval = timeval;
  Plibusb_timeval = ^Tlibusb_timeval;
{$EndIf}

{$if defined(CPU64)}
  Tssize = Int64;
 {$elseif defined(CPU32)}
  Tssize = Int32;
 {$else}
   {$ERROR  System Bit size should not defined!}
{$endif}

//  Tuint8_t = byte;

//  Tuint16_t = word;
//  Tuint32_t = dword;

//  CPUAMD64, CPUX86_64


{
   * Public libusb header file
   * Copyright © 2001 Johannes Erdfelt <johannes@erdfelt.com>
   * Copyright © 2007-2008 Daniel Drake <dsd@gentoo.org>
   * Copyright © 2012 Pete Batard <pete@akeo.ie>
   * Copyright © 2012 Nathan Hjelm <hjelmn@cs.unm.edu>
   * For more information, please visit: http://libusb.info
   *
   * This library is free software; you can redistribute it and/or
   * modify it under the terms of the GNU Lesser General Public
   * License as published by the Free Software Foundation; either
   * version 2.1 of the License, or (at your option) any later version.
   *
   * This library is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   * Lesser General Public License for more details.
   *
   * You should have received a copy of the GNU Lesser General Public
   * License along with this library; if not, write to the Free Software
   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
}

//{$ifdef WINDOWS}
//  { on MS environments, the inline keyword is available in C++ only  }
//{$if !defined(__cplusplus)}
//
//  const
//    inline = __inline;    
//{$endif}
//  { ssize_t is also not available (copy/paste from MinGW)  }
//{$ifndef _SSIZE_T_DEFINED}
//{$define _SSIZE_T_DEFINED}  
//{$undef ssize_t}
//{$ifdef _WIN64}
//  type
//    size_t = UINT64
//    Tssize_t = int64;
//{$else}
//  type
//    size_t = UINT32
//    Tssize_t = longint;
//{$endif}
//  { _WIN64  }
//{$endif}
//  { _SSIZE_T_DEFINED  }
//{$endif} { WINDOWS } { stdint.h is not available on older MSVC  }


//{$if defined(_MSC_VER) && (_MSC_VER < 1600) && (!defined(_STDINT)) && (!defined(_STDINT_H))}
//
//  type
//    Byte = byte;
//
//    Word = word;
//
//    LongWord = dword;
//{$else}
//{$include <stdint.h>}
//{$endif}
//{$if !defined(_WIN32_WCE)}
//{$include <sys/types.h>}
//{$endif}
//{$if defined(__linux__) || defined(__APPLE__) || defined(__CYGWIN__) || defined(__HAIKU__)}
//{$include <sys/time.h>}
//{$endif}
//{$include <time.h>}
//{$include <limits.h>}
//{$if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)}
//  { [] - valid C99 code  }
//{$define ZERO_SIZED_ARRAY}  
//{$else}
//  { [0] - non-standard, but usually working code  }
//
//  const
//    ZERO_SIZED_ARRAY = 0;    
//{$endif}
//  { 'interface' might be defined as a macro on Windows, so we need to
//   * undefine it so as not to break the current libusb API, because
//   * libusb_config_descriptor has an 'interface' member
//   * As this can be problematic if you include windows.h after libusb.h
//   * in your sources, we force windows.h to be included first.  }
//{$if defined(_WIN32) || defined(__CYGWIN__) || defined(_WIN32_WCE)}
//{$include <windows.h>}
//{$if defined(interface)}
//{$undef interface}
//{$endif}
//{$if !defined(__CYGWIN__)}
//{$include <winsock.h>}
//{$endif}
//{$endif}

//#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 5)
//#define LIBUSB_DEPRECATED_FOR(f) \
//  __attribute__((deprecated("Use " #f " instead")))
//#elif __GNUC__ >= 3
//#define LIBUSB_DEPRECATED_FOR(f) __attribute__((deprecated))
//#else
//#define LIBUSB_DEPRECATED_FOR(f)
//#endif /* __GNUC__ */
{ TODO : Deprecated noch nicht gelöst }
const deprecated_todo = '';

{ __GNUC__  }
{
  * \def LIBUSB_CALL
  * \ingroup libusb_misc
  * libusb's Windows calling convention.
  *
  * Under Windows, the selection of available compilers and configurations
  * means that, unlike other platforms, there is not <em>one true calling
  * convention</em> (calling convention: the manner in which parameters are
  * passed to functions in the generated assembly code).
  *
  * Matching the Windows API itself, libusb uses the WINAPI convention (which
  * translates to the <tt>stdcall</tt> convention) and guarantees that the
  * library is compiled in this way. The public header file also includes
  * appropriate annotations so that your own software will use the right
  * convention, even if another convention is being used by default within
  * your codebase.
  *
  * The one consideration that you must apply in your software is to mark
  * all functions which you use as libusb callbacks with this LIBUSB_CALL
  * annotation, so that they too get compiled for the correct calling
  * convention.
  *
  * On non-Windows operating systems, this macro is defined as nothing. This
  * means that you can apply it to your code without worrying about
  * cross-platform compatibility.
}
{
  * LIBUSB_CALL must be defined on both definition and declaration of libusb
  * functions. You'd think that declaration would be enough, but cygwin will
  * complain about conflicting types unless both are marked this way.
  * The placement of this macro is important too; it must appear after the
  * return type, before the function name. See internal documentation for
  * API_EXPORTED.
}
{$if defined(_WIN32) or defined(__CYGWIN__) or defined(_WIN32_WCE)}

    const
      LIBUSB_CALL = WINAPI;      
{$else}
{$define LIBUSB_CALL}    
{$endif}
{
  * \def LIBUSB_API_VERSION
  * \ingroup libusb_misc
  * libusb's API version.
  *
  * Since version 1.0.13, to help with feature detection, libusb defines
  * a LIBUSB_API_VERSION macro that gets increased every time there is a
  * significant change to the API, such as the introduction of a new call,
  * the definition of a new macro/enum member, or any other element that
  * libusb applications may want to detect at compilation time.
  *
  * The macro is typically used in an application as follows:
  * \code
  * #if defined(LIBUSB_API_VERSION) && (LIBUSB_API_VERSION >= 0x01001234)
  * // Use one of the newer features from the libusb API
  * #endif
  * \endcode
  *
  * Internally, LIBUSB_API_VERSION is defined as follows:
  * (libusb major << 24) | (libusb minor << 16) | (16 bit incremental)
}

const
  LIBUSB_API_VERSION = $01000106;
{ The following is kept for compatibility, but will be deprecated in the future  }
  LIBUSBX_API_VERSION = LIBUSB_API_VERSION;

var
  b16: Word; cvar;public;

{
  * standard USB stuff
  * \ingroup libusb_desc
  * Device and/or Interface Class codes
  * In the context of a \ref libusb_device_descriptor "device descriptor",
  * this bDeviceClass value indicates that each interface specifies its
  * own class information and all interfaces operate independently.
}
{
  * Audio class
  * Communications class
  * Human Interface Device class
  * Physical
  * Printer class
  * Image class
  * legacy name from libusb-0.1 usb.h
  * Mass storage class
  * Hub class
  * Data class
  * Smart Card
  * Content Security
  * Video
  * Personal Healthcare
  * Diagnostic Device
  * Wireless class
  * Application class
  * Class is vendor-specific
}

type
  {$PACKENUM 1}
  Tlibusb_class_code = (
    LIBUSB_CLASS_PER_INTERFACE := 0,LIBUSB_CLASS_AUDIO := 1,
    LIBUSB_CLASS_COMM := 2,LIBUSB_CLASS_HID := 3,
    LIBUSB_CLASS_PHYSICAL := 5,
//    LIBUSB_CLASS_PTP := 6,  libusb 0.1
    LIBUSB_CLASS_IMAGE := 6, LIBUSB_CLASS_PRINTER := 7,
    LIBUSB_CLASS_MASS_STORAGE := 8,LIBUSB_CLASS_HUB := 9,
    LIBUSB_CLASS_DATA := 10,LIBUSB_CLASS_SMART_CARD := $0b,
    LIBUSB_CLASS_CONTENT_SECURITY := $0d,
    LIBUSB_CLASS_VIDEO := $0e,LIBUSB_CLASS_PERSONAL_HEALTHCARE := $0f,
    LIBUSB_CLASS_DIAGNOSTIC_DEVICE := $dc,
    LIBUSB_CLASS_WIRELESS := $e0,LIBUSB_CLASS_APPLICATION := $fe,
    LIBUSB_CLASS_VENDOR_SPEC := $ff
  );

{
  * \ingroup libusb_desc
  * Descriptor types as defined by the USB specification.
  * Device descriptor. See libusb_device_descriptor.
  * Configuration descriptor. See libusb_config_descriptor.
  * String descriptor
  * Interface descriptor. See libusb_interface_descriptor.
  * Endpoint descriptor. See libusb_endpoint_descriptor.
  * BOS descriptor
  * Device Capability descriptor
  * HID descriptor
  * HID report descriptor
  * Physical descriptor
  * Hub descriptor
  * SuperSpeed Hub descriptor
  * SuperSpeed Endpoint Companion descriptor
}
  {$PACKENUM 1}
  Tlibusb_descriptor_type = (
    LIBUSB_DT_DEVICE := $01,LIBUSB_DT_CONFIG := $02,
    LIBUSB_DT_STRING := $03,LIBUSB_DT_INTERFACE := $04,
    LIBUSB_DT_ENDPOINT := $05,LIBUSB_DT_BOS := $0f,
    LIBUSB_DT_DEVICE_CAPABILITY := $10,
    LIBUSB_DT_HID := $21,LIBUSB_DT_REPORT := $22,
    LIBUSB_DT_PHYSICAL := $23,LIBUSB_DT_HUB := $29,
    LIBUSB_DT_SUPERSPEED_HUB := $2a,LIBUSB_DT_SS_ENDPOINT_COMPANION := $30
  );

 { Descriptor sizes per descriptor type  }

const
  LIBUSB_DT_DEVICE_SIZE = 18;
  LIBUSB_DT_CONFIG_SIZE = 9;
  LIBUSB_DT_INTERFACE_SIZE = 9;
  LIBUSB_DT_ENDPOINT_SIZE = 7;
  { Audio extension  }
  LIBUSB_DT_ENDPOINT_AUDIO_SIZE = 9;
  LIBUSB_DT_HUB_NONVAR_SIZE = 7;
  LIBUSB_DT_SS_ENDPOINT_COMPANION_SIZE = 6;
  LIBUSB_DT_BOS_SIZE = 5;
  LIBUSB_DT_DEVICE_CAPABILITY_SIZE = 3;
  { BOS descriptor sizes  }
  LIBUSB_BT_USB_2_0_EXTENSION_SIZE = 7;
  LIBUSB_BT_SS_USB_DEVICE_CAPABILITY_SIZE = 10;
  LIBUSB_BT_CONTAINER_ID_SIZE = 20;
  { We unwrap the BOS => define its max size  }

  { was #define dname def_expr }
//  function LIBUSB_DT_BOS_MAX_SIZE : TLIBUSB_DT_BOS_SIZE;

const
  LIBUSB_DT_BOS_MAX_SIZE = LIBUSB_DT_BOS_SIZE
    + LIBUSB_BT_USB_2_0_EXTENSION_SIZE
    + LIBUSB_BT_SS_USB_DEVICE_CAPABILITY_SIZE
    + LIBUSB_BT_CONTAINER_ID_SIZE;

{ in bEndpointAddress  }
const
  LIBUSB_ENDPOINT_ADDRESS_MASK = $0f;
  LIBUSB_ENDPOINT_DIR_MASK = $80;
{
  * \ingroup libusb_desc
  * Endpoint direction. Values for bit 7 of the
  * \ref libusb_endpoint_descriptor::bEndpointAddress "endpoint address" scheme.
}
 {* In: device-to-host  }
 {* Out: host-to-device  }

type
  Tlibusb_endpoint_direction = (
    LIBUSB_ENDPOINT_OUT := $00, LIBUSB_ENDPOINT_IN := $80
  );

{ in bmAttributes  }

const
  LIBUSB_TRANSFER_TYPE_MASK = $03;
  LIBUSB_TRANSFER_TYPE_MASK_30 = $07; //For USB 3.0
  ZERO_SIZED_ARRAY = 0;
{
  * \ingroup libusb_desc
  * Endpoint transfer type. Values for bits 0:1 of the
  * \ref libusb_endpoint_descriptor::bmAttributes "endpoint attributes" field.
}
{
  * Control endpoint
  * Isochronous endpoint
  * Bulk endpoint
  * Interrupt endpoint
  * Stream endpoint
}

type
{ DONE : bmAttributtes als bitpacket Record umbauen }
  {$PACKENUM 1}
  Tlibusb_transfer_type = (
    LIBUSB_TRANSFER_TYPE_CONTROL := 0,
    LIBUSB_TRANSFER_TYPE_ISOCHRONOUS := 1,
    LIBUSB_TRANSFER_TYPE_BULK := 2,
    LIBUSB_TRANSFER_TYPE_INTERRUPT := 3
  );

// USB 3.0
{$PACKENUM 1}
  Tlibusb_transfer_type_30 = (
    LIBUSB_TRANSFER_TYPE_CONTROL_30 := 0,
    LIBUSB_TRANSFER_TYPE_ISOCHRONOUS_30 := 1,
    LIBUSB_TRANSFER_TYPE_BULK_30 := 2,
    LIBUSB_TRANSFER_TYPE_INTERRUPT_30 := 3,
    LIBUSB_TRANSFER_TYPE_BULK_STREAM_30 := 4
  );


{
  * \ingroup libusb_misc
  * Standard requests, as defined in table 9-5 of the USB 3.0 specifications
  * Request status of the specific recipient
  * Clear or disable a specific feature
  * 0x02 is reserved
  * Set or enable a specific feature
  * 0x04 is reserved
  * Set device address for all future accesses
  * Get the specified descriptor
  * Used to update existing descriptors or add new descriptors
  * Get the current device configuration value
  * Set device configuration
  * Return the selected alternate setting for the specified interface
  * Select an alternate interface for the specified interface
  * Set then report an endpoint's synchronization frame
  * Sets both the U1 and U2 Exit Latency
  * Delay from the time a host transmits a packet to the time it is
  * received by the device.
}
{$PACKENUM 1}
  Tlibusb_standard_request = (
    LIBUSB_REQUEST_GET_STATUS := $00,
    LIBUSB_REQUEST_CLEAR_FEATURE := $01,
    LIBUSB_REQUEST_SET_FEATURE := $03,
    LIBUSB_REQUEST_SET_ADDRESS := $05,
    LIBUSB_REQUEST_GET_DESCRIPTOR := $06,
    LIBUSB_REQUEST_SET_DESCRIPTOR := $07,
    LIBUSB_REQUEST_GET_CONFIGURATION := $08,
    LIBUSB_REQUEST_SET_CONFIGURATION := $09,
    LIBUSB_REQUEST_GET_INTERFACE := $0A,
    LIBUSB_REQUEST_SET_INTERFACE := $0B,
    LIBUSB_REQUEST_SYNCH_FRAME := $0C,
    LIBUSB_REQUEST_SET_SEL := $30,
    LIBUSB_SET_ISOCH_DELAY := $31
  );

{
  * \ingroup libusb_misc
  * Request type bits of the
  * \ref libusb_control_setup::bmRequestType "bmRequestType" field in control
  * transfers.
  * Standard
  * Class
  * Vendor
  * Reserved
}
  Tlibusb_request_type = (
    LIBUSB_REQUEST_TYPE_STANDARD := $00 shl 5,LIBUSB_REQUEST_TYPE_CLASS := $01 shl 5,
    LIBUSB_REQUEST_TYPE_VENDOR := $02 shl 5,LIBUSB_REQUEST_TYPE_RESERVED := $03 shl 5
  );

{
  * \ingroup libusb_misc
  * Recipient bits of the
  * \ref libusb_control_setup::bmRequestType "bmRequestType" field in control
  * transfers. Values 4 through 31 are reserved.
  * Device
  * Interface
  * Endpoint
  * Other
}
  Tlibusb_request_recipient = (
    LIBUSB_RECIPIENT_DEVICE := $00,LIBUSB_RECIPIENT_INTERFACE := $01,
    LIBUSB_RECIPIENT_ENDPOINT := $02,LIBUSB_RECIPIENT_OTHER := $03
  );


const
  LIBUSB_ISO_SYNC_TYPE_MASK = $0C;
  LIBUSB_ISO_SYNC_TYPE_BITS_OFFSET = 2;
{
  * \ingroup libusb_desc
  * Synchronization type for isochronous endpoints. Values for bits 2:3 of the
  * \ref libusb_endpoint_descriptor::bmAttributes "bmAttributes" field in
  * libusb_endpoint_descriptor.
}
 {* No synchronization  }
 {* Asynchronous  }
 {* Adaptive  }
 {* Synchronous  }

type
  {$PACKENUM 1}
  Tlibusb_iso_sync_type = (
    LIBUSB_ISO_SYNC_TYPE_NONE := 0 shl LIBUSB_ISO_SYNC_TYPE_BITS_OFFSET,
    LIBUSB_ISO_SYNC_TYPE_ASYNC := 1 shl LIBUSB_ISO_SYNC_TYPE_BITS_OFFSET,
    LIBUSB_ISO_SYNC_TYPE_ADAPTIVE := 2 shl LIBUSB_ISO_SYNC_TYPE_BITS_OFFSET,
    LIBUSB_ISO_SYNC_TYPE_SYNC := 3 shl LIBUSB_ISO_SYNC_TYPE_BITS_OFFSET
  );


const
  LIBUSB_ISO_USAGE_TYPE_MASK = $30;
  LIBUSB_ISO_USAGE_TYPE_BITS_OFFSET = 4;
{
  * \ingroup libusb_desc
  * Usage type for isochronous endpoints. Values for bits 4:5 of the
  * \ref libusb_endpoint_descriptor::bmAttributes "bmAttributes" field in
  * libusb_endpoint_descriptor.
}
 {* Data endpoint  }
 {* Feedback endpoint  }
 {* Implicit feedback Data endpoint  }

type
  {$PACKENUM 1}
  Tlibusb_iso_usage_type = (
    LIBUSB_ISO_USAGE_TYPE_DATA := 0 shl LIBUSB_ISO_USAGE_TYPE_BITS_OFFSET,
    LIBUSB_ISO_USAGE_TYPE_FEEDBACK := 1 shl LIBUSB_ISO_USAGE_TYPE_BITS_OFFSET,
    LIBUSB_ISO_USAGE_TYPE_IMPLICIT := 2 shl LIBUSB_ISO_USAGE_TYPE_BITS_OFFSET
  );

{
  * \ingroup libusb_desc
  * A structure representing the standard USB device descriptor. This
  * descriptor is documented in section 9.6.1 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_DEVICE LIBUSB_DT_DEVICE in this
  * context.
  * USB specification release number in binary-coded decimal. A value of
  * 0x0200 indicates USB 2.0, 0x0110 indicates USB 1.1, etc.
  * USB-IF class code for the device. See \ref libusb_class_code.
  * USB-IF subclass code for the device, qualified by the bDeviceClass
  * value
  * USB-IF protocol code for the device, qualified by the bDeviceClass and
  * bDeviceSubClass values
  * Maximum packet size for endpoint 0
  * USB-IF vendor ID
  * USB-IF product ID
  * Device release number in binary-coded decimal
  * Index of string descriptor describing manufacturer
  * Index of string descriptor describing product
  * Index of string descriptor containing device serial number
  * Number of possible configurations
}
  Tlibusb_device_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bcdUSB : Word;
    bDeviceClass : Tlibusb_class_code;
    bDeviceSubClass : Byte;
    bDeviceProtocol : Byte;
    bMaxPacketSize0 : Byte;
    idVendor : Word;
    idProduct : Word;
    bcdDevice : Word;
    iManufacturer : Byte;
    iProduct : Byte;
    iSerialNumber : Byte;
    bNumConfigurations : Byte;
  end;
  Plibusb_device_descriptor = ^Tlibusb_device_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the standard USB endpoint descriptor. This
  * descriptor is documented in section 9.6.6 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_ENDPOINT LIBUSB_DT_ENDPOINT in
  * this context.
}
{
  * The address of the endpoint described by this descriptor. Bits 0:3 are
  * the endpoint number. Bits 4:6 are reserved. Bit 7 indicates direction,
  * see \ref libusb_endpoint_direction.
}
{
  * Attributes which apply to the endpoint when it is configured using
  * the bConfigurationValue. Bits 0:1 determine the transfer type and
  * correspond to \ref libusb_transfer_type. Bits 2:3 are only used for
  * isochronous endpoints and correspond to \ref libusb_iso_sync_type.
  * Bits 4:5 are also only used for isochronous endpoints and correspond to
  * \ref libusb_iso_usage_type. Bits 6:7 are reserved.
}
{ * Maximum packet size this endpoint is capable of sending/receiving.
  * Interval for polling endpoint for data transfers.
  * For audio devices only: the rate at which synchronization feedback
  * is provided.
  * For audio devices only: the address if the synch endpoint
  * Extra descriptors. If libusb encounters unknown endpoint descriptors,
  * it will store them here, should you wish to parse them.
  * Const before type ignored
  * Length of the extra descriptors, in bytes.
}
  Tlibusb_endpoint_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bEndpointAddress : Byte;
    bmAttributes : Byte;
    wMaxPacketSize : Word;
    bInterval : Byte;
    bRefresh : Byte;
    bSynchAddress : Byte;
    extra : PByte;
    extra_length : longint;
  end;
  Plibusb_endpoint_descriptor = ^Tlibusb_endpoint_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the standard USB interface descriptor. This
  * descriptor is documented in section 9.6.5 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_INTERFACE LIBUSB_DT_INTERFACE
  * in this context.
  * Number of this interface
  * Value used to select this alternate setting for this interface
  * Number of endpoints used by this interface (excluding the control
  * endpoint).
  * USB-IF class code for this interface. See \ref libusb_class_code.
  * USB-IF subclass code for this interface, qualified by the
  * bInterfaceClass value
  * USB-IF protocol code for this interface, qualified by the
  * bInterfaceClass and bInterfaceSubClass values
  * Index of string descriptor describing this interface
  * Array of endpoint descriptors. This length of this array is determined
  * by the bNumEndpoints field.
  * Const before type ignored
  * Extra descriptors. If libusb encounters unknown interface descriptors,
  * it will store them here, should you wish to parse them.
  * Const before type ignored
  * Length of the extra descriptors, in bytes.
}
  Tlibusb_interface_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bInterfaceNumber : Byte;
    bAlternateSetting : Byte;
    bNumEndpoints : Byte;
    bInterfaceClass : Byte;
    bInterfaceSubClass : Byte;
    bInterfaceProtocol : Byte;
    iInterface : Byte;
    endpoint : Plibusb_endpoint_descriptor;
    extra : PByte;
    extra_length : longint;
  end;
  Plibusb_interface_descriptor = ^Tlibusb_interface_descriptor;

{
  * \ingroup libusb_desc
  * A collection of alternate settings for a particular USB interface.
}
{
  * Array of interface descriptors. The length of this array is determined
  * by the num_altsetting field.
  * Const before type ignored
  * The number of alternate settings that belong to this interface
}
  Tlibusb_interface = record
    altsetting : ^Tlibusb_interface_descriptor;
    num_altsetting : longint;
  end;

{
  * \ingroup libusb_desc
  * A structure representing the standard USB configuration descriptor. This
  * descriptor is documented in section 9.6.3 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_CONFIG LIBUSB_DT_CONFIG
  * in this context.
  * Total length of data returned for this configuration
  * Number of interfaces supported by this configuration
  * Identifier value for this configuration
  * Index of string descriptor describing this configuration
  * Configuration characteristics
  * Maximum power consumption of the USB device from this bus in this
  * configuration when the device is fully operation. Expressed in units
  * of 2 mA when the device is operating in high-speed mode and in units
  * of 8 mA when the device is operating in super-speed mode.
  * Array of interfaces supported by this configuration. The length of
  * this array is determined by the bNumInterfaces field.
  * Const before type ignored
  * Extra descriptors. If libusb encounters unknown configuration
  * descriptors, it will store them here, should you wish to parse them.
  * Const before type ignored
  * Length of the extra descriptors, in bytes.
}
  Tlibusb_config_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    wTotalLength : Word;
    bNumInterfaces : Byte;
    bConfigurationValue : Byte;
    iConfiguration : Byte;
    bmAttributes : Byte;
    MaxPower : Byte;
    usb_interface : ^Tlibusb_interface;
    extra : PByte;
    extra_length : longint;
  end;
  Plibusb_config_descriptor = ^Tlibusb_config_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the superspeed endpoint companion
  * descriptor. This descriptor is documented in section 9.6.7 of
  * the USB 3.0 specification. All multiple-byte fields are represented in
  * host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_SS_ENDPOINT_COMPANION in
  * this context.
  * The maximum number of packets the endpoint can send or
  * receive as part of a burst.
  * In bulk EP:	bits 4:0 represents the	maximum	number of
  * streams the EP supports. In isochronous EP: bits 1:0
  * represents the Mult - a zero based value that determines
  * the maximum number of packets within a service interval
  * The total number of bytes this EP will transfer every
  * service interval. valid only for periodic EPs.
}
  Tlibusb_ss_endpoint_companion_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bMaxBurst : Byte;
    bmAttributes : Byte;
    wBytesPerInterval : Word;
  end;
  Plibusb_ss_endpoint_companion_descriptor = ^Tlibusb_ss_endpoint_companion_descriptor;

  {* \ingroup libusb_desc
   * A generic representation of a BOS Device Capability descriptor. It is
   * advised to check bDevCapabilityType and call the matching
   * libusb_get_*_descriptor function to get a structure fully matching the type.
    }
  {* Size of this descriptor (in bytes)  }
  {* Descriptor type. Will have value
  	 * \ref libusb_descriptor_type::LIBUSB_DT_DEVICE_CAPABILITY
  	 * LIBUSB_DT_DEVICE_CAPABILITY in this context.  }
  {* Device Capability type  }
  {* Device Capability data (bLength - 3 bytes)  }
  Tlibusb_bos_dev_capability_descriptor = record
      bLength : Byte;
      bDescriptorType : Tlibusb_descriptor_type;
      bDevCapabilityType : Byte;
      dev_capability_data : array of Byte;
    end;
  Plibusb_bos_dev_capability_descriptor = ^Tlibusb_bos_dev_capability_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the Binary Device Object Store (BOS) descriptor.
  * This descriptor is documented in section 9.6.2 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_BOS LIBUSB_DT_BOS
  * in this context.
  * Length of this descriptor and all of its sub descriptors
  * The number of separate device capability descriptors in
  * the BOS
  * bNumDeviceCap Device Capability Descriptors
}
  Tlibusb_bos_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    wTotalLength : Word;
    bNumDeviceCaps : Byte;
    dev_capability : array of ^Tlibusb_bos_dev_capability_descriptor;
  end;
  Plibusb_bos_descriptor = ^Tlibusb_bos_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the USB 2.0 Extension descriptor
  * This descriptor is documented in section 9.6.2.1 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_DEVICE_CAPABILITY
  * LIBUSB_DT_DEVICE_CAPABILITY in this context.
  * Capability type. Will have value
  * \ref libusb_capability_type::LIBUSB_BT_USB_2_0_EXTENSION
  * LIBUSB_BT_USB_2_0_EXTENSION in this context.
  * Bitmap encoding of supported device level features.
  * A value of one in a bit location indicates a feature is
  * supported; a value of zero indicates it is not supported.
  * See \ref libusb_usb_2_0_extension_attributes.
}
  Tlibusb_usb_2_0_extension_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bDevCapabilityType : Byte;
    bmAttributes : LongWord;
  end;
  Plibusb_usb_2_0_extension_descriptor = ^Tlibusb_usb_2_0_extension_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the SuperSpeed USB Device Capability descriptor
  * This descriptor is documented in section 9.6.2.2 of the USB 3.0 specification.
  * All multiple-byte fields are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_DEVICE_CAPABILITY
  * LIBUSB_DT_DEVICE_CAPABILITY in this context.
  * Capability type. Will have value
  * \ref libusb_capability_type::LIBUSB_BT_SS_USB_DEVICE_CAPABILITY
  * LIBUSB_BT_SS_USB_DEVICE_CAPABILITY in this context.
  * Bitmap encoding of supported device level features.
  * A value of one in a bit location indicates a feature is
  * supported; a value of zero indicates it is not supported.
  * See \ref libusb_ss_usb_device_capability_attributes.
  * Bitmap encoding of the speed supported by this device when
  * operating in SuperSpeed mode. See \ref libusb_supported_speed.
  * The lowest speed at which all the functionality supported
  * by the device is available to the user. For example if the
  * device supports all its functionality when connected at
  * full speed and above then it sets this value to 1.
  * U1 Device Exit Latency.
  * U2 Device Exit Latency.
}
  Tlibusb_ss_usb_device_capability_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bDevCapabilityType : Byte;
    bmAttributes : Byte;
    wSpeedSupported : Word;
    bFunctionalitySupport : Byte;
    bU1DevExitLat : Byte;
    bU2DevExitLat : Word;
  end;
  Plibusb_ss_usb_device_capability_descriptor = ^Tlibusb_ss_usb_device_capability_descriptor;

{
  * \ingroup libusb_desc
  * A structure representing the Container ID descriptor.
  * This descriptor is documented in section 9.6.2.3 of the USB 3.0 specification.
  * All multiple-byte fields, except UUIDs, are represented in host-endian format.
}
{
  * Size of this descriptor (in bytes)
  * Descriptor type. Will have value
  * \ref libusb_descriptor_type::LIBUSB_DT_DEVICE_CAPABILITY
  * LIBUSB_DT_DEVICE_CAPABILITY in this context.
  * Capability type. Will have value
  * \ref libusb_capability_type::LIBUSB_BT_CONTAINER_ID
  * LIBUSB_BT_CONTAINER_ID in this context.
  * Reserved field
  * 128 bit UUID
}
  Tlibusb_container_id_descriptor = record
    bLength : Byte;
    bDescriptorType : Tlibusb_descriptor_type;
    bDevCapabilityType : Byte;
    bReserved : Byte;
    ContainerID : array[0..15] of Byte;
  end;
  Plibusb_container_id_descriptor = ^Tlibusb_container_id_descriptor;

{
  * \ingroup libusb_asyncio
  * Setup packet for control transfers.
  * Request type. Bits 0:4 determine recipient, see
  * \ref libusb_request_recipient. Bits 5:6 determine type, see
  * \ref libusb_request_type. Bit 7 determines data transfer direction, see
  * \ref libusb_endpoint_direction.
  * Request. If the type bits of bmRequestType are equal to
  * \ref libusb_request_type::LIBUSB_REQUEST_TYPE_STANDARD
  * "LIBUSB_REQUEST_TYPE_STANDARD" then this field refers to
  * \ref libusb_standard_request. For other cases, use of this field is
  * application-specific.
  * Value. Varies according to request
  * Index. Varies according to request, typically used to pass an index
  * or offset
  * Number of bytes to transfer
}
  Tlibusb_control_setup = record
    bmRequestType : Byte;
    bRequest : Byte;
    wValue : Word;
    wIndex : Word;
    wLength : Word;
  end;
  Plibusb_control_setup = ^Tlibusb_control_setup;

const
  LIBUSB_CONTROL_SETUP_SIZE = sizeof(Tlibusb_control_setup);

type
  { libusb  }
  Tlibusb_context = record
      {undefined structure}
  end;
  Plibusb_context = ^Tlibusb_context;

  Tlibusb_device = record
      {undefined structure}
  end;
  Plibusb_device = ^Tlibusb_device;
  PPlibusb_device = ^Plibusb_device;

  Tlibusb_device_handle = record
      {undefined structure}
  end;
  Plibusb_device_handle = ^Tlibusb_device_handle;

{
  * \ingroup libusb_lib
  * Structure providing the version of the libusb runtime
  * Library major version.
  * Const before type ignored
  * Library minor version.
  * Const before type ignored
  * Library micro version.
  * Const before type ignored
  * Library nano version.
  * Const before type ignored
  * Library release candidate suffix string, e.g. "-rc4".
  * Const before type ignored
  * For ABI compatibility only.
  * Const before type ignored
}

  Tlibusb_version = record
    major : Word;
    minor : Word;
    micro : Word;
    nano : Word;
    rc : ^char;
    describe : ^char;
  end;
  Plibusb_version = ^Tlibusb_version;

{
  * \ingroup libusb_lib
  * Structure representing a libusb session. The concept of individual libusb
  * sessions allows for your program to use two libraries (or dynamically
  * load two modules) which both independently use libusb. This will prevent
  * interference between the individual libusb users - for example
  * libusb_set_option() will not affect the other user of the library, and
  * libusb_exit() will not destroy resources that the other user is still
  * using.
  *
  * Sessions are created by libusb_init() and destroyed through libusb_exit().
  * If your application is guaranteed to only ever include a single libusb
  * user (i.e. you), you do not have to worry about contexts: pass NULL in
  * every function call where a context is required. The default context
  * will be used.
  *
  * For more information, see \ref libusb_contexts.
}
{
  * \ingroup libusb_dev
  * Structure representing a USB device detected on the system. This is an
  * opaque type for which you are only ever provided with a pointer, usually
  * originating from libusb_get_device_list().
  *
  * Certain operations can be performed on a device, but in order to do any
  * I/O you will have to first obtain a device handle using libusb_open().
  *
  * Devices are reference counted with libusb_ref_device() and
  * libusb_unref_device(), and are freed when the reference count reaches 0.
  * New devices presented by libusb_get_device_list() have a reference count of
  * 1, and libusb_free_device_list() can optionally decrease the reference count
  * on all devices in the list. libusb_open() adds another reference which is
  * later destroyed by libusb_close().
}
{
  * \ingroup libusb_dev
  * Structure representing a handle on a USB device. This is an opaque type for
  * which you are only ever provided with a pointer, usually originating from
  * libusb_open().
  *
  * A device handle is used to perform I/O and other operations. When finished
  * with a device handle, you should call libusb_close().
}
{
  * \ingroup libusb_dev
  * Speed codes. Indicates the speed at which the device is operating.
}
{
  * The OS doesn't report or know the device speed.
  * The device is operating at low speed (1.5MBit/s).
  * The device is operating at full speed (12MBit/s).
  * The device is operating at high speed (480MBit/s).
  * The device is operating at super speed (5000MBit/s).
  * The device is operating at super speed plus (10000MBit/s).
}
  Tlibusb_speed = (
    LIBUSB_SPEED_UNKNOWN := 0,LIBUSB_SPEED_LOW := 1,
    LIBUSB_SPEED_FULL := 2,LIBUSB_SPEED_HIGH := 3,
    LIBUSB_SPEED_SUPER := 4,LIBUSB_SPEED_SUPER_PLUS := 5
  );

{
  * \ingroup libusb_dev
  * Supported speeds (wSpeedSupported) bitfield. Indicates what
  * speeds the device supports.
}
{
  * Low speed operation supported (1.5MBit/s).
  * Full speed operation supported (12MBit/s).
  * High speed operation supported (480MBit/s).
  * Superspeed operation supported (5000MBit/s).
}
  Tlibusb_supported_speed = (
    LIBUSB_LOW_SPEED_OPERATION := 1,LIBUSB_FULL_SPEED_OPERATION := 2,
    LIBUSB_HIGH_SPEED_OPERATION := 4,
    LIBUSB_SUPER_SPEED_OPERATION := 8
    );

  {* \ingroup libusb_dev
   * Masks for the bits of the
   * \ref libusb_usb_2_0_extension_descriptor::bmAttributes "bmAttributes" field
   * of the USB 2.0 Extension descriptor.}
  {* Supports Link Power Management (LPM)  }
  Tlibusb_usb_2_0_extension_attributes = (LIBUSB_BM_LPM_SUPPORT := 2);

{
  * \ingroup libusb_dev
  * Masks for the bits of the
  * \ref libusb_ss_usb_device_capability_descriptor::bmAttributes "bmAttributes" field
  * field of the SuperSpeed USB Device Capability descriptor.
  * Supports Latency Tolerance Messages (LTM)
}
  Tlibusb_ss_usb_device_capability_attributes = (LIBUSB_BM_LTM_SUPPORT := 2);

{
  * \ingroup libusb_dev
  * USB capability types
  * Wireless USB device capability
  * USB 2.0 extensions
  * SuperSpeed USB device capability
  *Container ID type
}
  Tlibusb_bos_type = (
    LIBUSB_BT_WIRELESS_USB_DEVICE_CAPABILITY := 1,
    LIBUSB_BT_USB_2_0_EXTENSION := 2,
    LIBUSB_BT_SS_USB_DEVICE_CAPABILITY := 3,
    LIBUSB_BT_CONTAINER_ID := 4
  );

{
  * \ingroup libusb_misc
  * Error codes. Most libusb functions return 0 on success or one of these
  * codes on failure.
  * You can call libusb_error_name() to retrieve a string representation of an
  * error code or libusb_strerror() to get an end-user suitable description of
  * an error code.
  * Success (no error)
  * Input/output error
  * Invalid parameter
  * Access denied (insufficient permissions)
  * No such device (it may have been disconnected)
  * Entity not found
  * Resource busy
  * Operation timed out
  * Overflow
  * Pipe error
  * System call interrupted (perhaps due to signal)
  * Insufficient memory
  * Operation not supported or unimplemented on this platform
  * NB: Remember to update LIBUSB_ERROR_COUNT below as well as the
  * message strings in strerror.c when adding new error codes here.
  * Other error
}
  Tlibusb_error = (
    LIBUSB_ERROR_OTHER := -(99), LIBUSB_ERROR_NOT_SUPPORTED := -(12),
    LIBUSB_ERROR_NO_MEM := -(11), LIBUSB_ERROR_INTERRUPTED := -(10),
    LIBUSB_ERROR_PIPE := -(9), LIBUSB_ERROR_OVERFLOW := -(8),
    LIBUSB_ERROR_TIMEOUT := -(7), LIBUSB_ERROR_BUSY := -(6),
    LIBUSB_ERROR_NOT_FOUND := -(5), LIBUSB_ERROR_NO_DEVICE := -(4),
    LIBUSB_ERROR_ACCESS := -(3), LIBUSB_ERROR_INVALID_PARAM := -(2),
    LIBUSB_ERROR_IO := -(1), LIBUSB_SUCCESS := 0
  );

  { Total number of error codes in enum libusb_error  }

const
  LIBUSB_ERROR_COUNT = 14;
{
  * \ingroup libusb_asyncio
  * Transfer status codes
  * Transfer completed without error. Note that this does not indicate
  * that the entire amount of requested data was transferred.
  * Transfer failed
  * Transfer timed out
  * Transfer was cancelled
  * For bulk/interrupt endpoints: halt condition detected (endpoint
  * stalled). For control endpoints: control request not supported.
  * Device was disconnected
  * Device sent more data than requested
  * NB! Remember to update libusb_error_name()
    	 when adding new status codes here.
}

type
  Tlibusb_transfer_status = (
    LIBUSB_TRANSFER_COMPLETED,LIBUSB_TRANSFER_ERROR,
    LIBUSB_TRANSFER_TIMED_OUT,LIBUSB_TRANSFER_CANCELLED,
    LIBUSB_TRANSFER_STALL,LIBUSB_TRANSFER_NO_DEVICE,
    LIBUSB_TRANSFER_OVERFLOW
  );

{
  * \ingroup libusb_asyncio
  * libusb_transfer.flags values
  * Report short frames as errors
  * Automatically free() transfer buffer during libusb_free_transfer().
  * Note that buffers allocated with libusb_dev_mem_alloc() should not
  * be attempted freed in this way, since free() is not an appropriate
  * way to release such memory.
  * Automatically call libusb_free_transfer() after callback returns.
  * If this flag is set, it is illegal to call libusb_free_transfer()
  * from your transfer callback, as this will result in a double-free
  * when this flag is acted upon.
  * Terminate transfers that are a multiple of the endpoint's
  * wMaxPacketSize with an extra zero length packet. This is useful
  * when a device protocol mandates that each logical request is
  * terminated by an incomplete packet (i.e. the logical requests are
  * not separated by other means).
  *
  * This flag only affects host-to-device transfers to bulk and interrupt
  * endpoints. In other situations, it is ignored.
  *
  * This flag only affects transfers with a length that is a multiple of
  * the endpoint's wMaxPacketSize. On transfers of other lengths, this
  * flag has no effect. Therefore, if you are working with a device that
  * needs a ZLP whenever the end of the logical request falls on a packet
  * boundary, then it is sensible to set this flag on <em>every</em>
  * transfer (you do not have to worry about only setting it on transfers
  * that end on the boundary).
  *
  * This flag is currently only supported on Linux.
  * On other systems, libusb_submit_transfer() will return
  * LIBUSB_ERROR_NOT_SUPPORTED for every transfer where this flag is set.
  *
  * Available since libusb-1.0.9.
}
  Tlibusb_transfer_flags = (
    LIBUSB_TRANSFER_SHORT_NOT_OK := 1 shl 0,
    LIBUSB_TRANSFER_FREE_BUFFER := 1 shl 1,
    LIBUSB_TRANSFER_FREE_TRANSFER := 1 shl 2,
    LIBUSB_TRANSFER_ADD_ZERO_PACKET := 1 shl 3
  );

{
  * \ingroup libusb_asyncio
  * Isochronous packet descriptor.
  * Length of data to request in this packet
  * Amount of data that was actually transferred
  * Status code for this packet
}
  Tlibusb_iso_packet_descriptor = record
      length : dword;
      actual_length : dword;
      status : Tlibusb_transfer_status;
    end;

{
  * \ingroup libusb_asyncio
  * Asynchronous transfer callback function type. When submitting asynchronous
  * transfers, you pass a pointer to a callback function of this type via the
  * \ref libusb_transfer::callback "callback" member of the libusb_transfer
  * structure. libusb will call this function later, when the transfer has
  * completed or failed. See \ref libusb_asyncio for more information.
  * \param transfer The libusb_transfer struct the callback function is being
  * notified about.
}

{
  * \ingroup libusb_asyncio
  * The generic USB transfer structure. The user populates this structure and
  * then submits it in order to request a transfer. After the transfer has
  * completed, the library populates the transfer with the results and passes
  * it back to the user.
  * Handle of the device that this transfer will be submitted to
  * A bitwise OR combination of \ref libusb_transfer_flags.
  * Address of the endpoint where this transfer will be sent.
  * Type of the endpoint from \ref libusb_transfer_type
  * Timeout for this transfer in milliseconds. A value of 0 indicates no
  * timeout.
  * The status of the transfer. Read-only, and only for use within
  * transfer callback function.
  *
  * If this is an isochronous transfer, this field may read COMPLETED even
  * if there were errors in the frames. Use the
  * \ref libusb_iso_packet_descriptor::status "status" field in each packet
  * to determine if errors occurred.
  * Length of the data buffer
  * Actual length of data that was transferred. Read-only, and only for
  * use within transfer callback function. Not valid for isochronous
  * endpoint transfers.
  * Callback function. This will be invoked when the transfer completes,
  * fails, or is cancelled.
  * User context data to pass to the callback function.
  * Data buffer
  * Number of isochronous packets. Only used for I/O with isochronous
  * endpoints.
  * Isochronous packet descriptors, for isochronous transfers only.
}
const
  LIBUSB_MAX_ISO_PACKET_DESCRIPTOR = 255;

{
  TODO : Workaround für "iso_packet_des"
  Bei der Deklaration "array of type" wird ein Zeiger hinterlegt,
  was bei libusb so nicht gemeint ist. Das array folgt im Anschluss
  an num_iso_packets.

  Sollte die Anzahl an iso-packet nicht reichen, muss die Konstante
  "LIBUSB_MAX_ISO_PACKET_DESCRIPTOR" hoch gesetzt werden.
}

type
  Tlibusb_transfer = record
      dev_handle : ^Tlibusb_device_handle;
      flags : Byte;
      endpoint : Byte;
      type_ : Byte;
      timeout : dword;
      status : Tlibusb_transfer_status;
      length : longint;
      actual_length : longint;
      callback : procedure(var transfer: Tlibusb_transfer) cdecl;
      user_data : pointer;
      buffer : PByte;
      num_iso_packets : longint;
      iso_packet_desc : array [0..LIBUSB_MAX_ISO_PACKET_DESCRIPTOR] of Tlibusb_iso_packet_descriptor;
//      iso_packet_desc : array of Tlibusb_iso_packet_descriptor;
    end;
  Plibusb_transfer = ^Tlibusb_transfer;
  Tlibusb_transfer_cb_fn = procedure(var transfer: Tlibusb_transfer); cdecl;

{
  * \ingroup libusb_misc
  * Capabilities supported by an instance of libusb on the current running
  * platform. Test if the loaded library supports a given capability by calling
  * \ref libusb_has_capability().
  * The libusb_has_capability() API is available.
  * Hotplug support is available on this platform.
  * The library can access HID devices without requiring user intervention.
  * Note that before being able to actually access an HID device, you may
  * still have to call additional libusb functions such as
  * \ref libusb_detach_kernel_driver().
  * The library supports detaching of the default USB driver, using
  * \ref libusb_detach_kernel_driver(), if one is set by the OS kernel
}
  Tlibusb_capability = (
    LIBUSB_CAP_HAS_CAPABILITY := $0000,LIBUSB_CAP_HAS_HOTPLUG := $0001,
    LIBUSB_CAP_HAS_HID_ACCESS := $0100,LIBUSB_CAP_SUPPORTS_DETACH_KERNEL_DRIVER := $0101
  );

{
  * \ingroup libusb_lib
  *  Log message levels.
  *  - LIBUSB_LOG_LEVEL_NONE (0)    : no messages ever printed by the library (default)
  *  - LIBUSB_LOG_LEVEL_ERROR (1)   : error messages are printed to stderr
  *  - LIBUSB_LOG_LEVEL_WARNING (2) : warning and error messages are printed to stderr
  *  - LIBUSB_LOG_LEVEL_INFO (3)    : informational messages are printed to stderr
  *  - LIBUSB_LOG_LEVEL_DEBUG (4)   : debug and informational messages are printed to stderr
}
  Tlibusb_log_level = (
    LIBUSB_LOG_LEVEL_NONE := 0,LIBUSB_LOG_LEVEL_ERROR := 1,
    LIBUSB_LOG_LEVEL_WARNING := 2,LIBUSB_LOG_LEVEL_INFO := 3,
    LIBUSB_LOG_LEVEL_DEBUG := 4
  );

{
  * \ingroup libusb_lib
  * Available option values for libusb_set_option().
  *
  * Set the log message verbosity.
  *
  * The default level is LIBUSB_LOG_LEVEL_NONE, which means no messages are ever
  * printed. If you choose to increase the message verbosity level, ensure
  * that your application does not close the stderr file descriptor.
  *
  * You are advised to use level LIBUSB_LOG_LEVEL_WARNING. libusb is conservative
  * with its message logging and most of the time, will only log messages that
  * explain error conditions and other oddities. This will help you debug
  * your software.
  *
  * If the LIBUSB_DEBUG environment variable was set when libusb was
  * initialized, this function does nothing: the message verbosity is fixed
  * to the value in the environment variable.
  *
  * If libusb was compiled without any message logging, this function does
  * nothing: you'll never get any messages.
  *
  * If libusb was compiled with verbose debug message logging, this function
  * does nothing: you'll always get messages from all levels.
  *
  * Use the UsbDk backend for a specific context, if available.
  *
  * This option should be set immediately after calling libusb_init(), otherwise
  * unspecified behavior may occur.
  *
  * Only valid on Windows.
}

  Tlibusb_option = (LIBUSB_OPTION_LOG_LEVEL, LIBUSB_OPTION_USE_USBDK);

{
  * \ingroup libusb_hotplug
  * Callback handle.
  *
  * Callbacks handles are generated by libusb_hotplug_register_callback()
  * and can be used to deregister callbacks. Callback handles are unique
  * per libusb_context and it is safe to call libusb_hotplug_deregister_callback()
  * on an already deregisted callback.
  *
  * Since version 1.0.16, \ref LIBUSB_API_VERSION >= 0x01000102
  *
  * For more information, see \ref libusb_hotplug.
}
    Tlibusb_hotplug_callback_handle = longint;

{
  * \ingroup libusb_hotplug
  *
  * Since version 1.0.16, \ref LIBUSB_API_VERSION >= 0x01000102
  *
  * Flags for hotplug events
  * Default value when not using any flags.
  * Arm the callback and fire it for all matching currently attached devices.
}
  Tlibusb_hotplug_flag = (
    LIBUSB_HOTPLUG_NO_FLAGS := 0,
    LIBUSB_HOTPLUG_ENUMERATE := 1 shl 0
  );

{
  * \ingroup libusb_hotplug
  *
  * Since version 1.0.16, \ref LIBUSB_API_VERSION >= 0x01000102
  *
  * Hotplug events
}
{
  * A device has been plugged in and is ready to use
  * A device has left and is no longer available.
  * It is the user's responsibility to call libusb_close on any handle
  * associated with a disconnected device.
  * It is safe to call libusb_get_device_descriptor on a device that has left
}

// Die Wertigkeit um 1 reduziert, da es sich in FPC jetzt um ein
// "set of" also bitset handelt.
    Tlibusb_hotplug_event = (
      LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED := $00,
      LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT := $01);
    Tlibusb_hotplug_events = set of Tlibusb_hotplug_event;

{
  * \ingroup libusb_hotplug
  * Wildcard matching for hotplug events
}
  const
    LIBUSB_HOTPLUG_MATCH_ANY = -(1);

{
  * \ingroup libusb_hotplug
  * Hotplug callback function type. When requesting hotplug event notifications,
  * you pass a pointer to a callback function of this type.
  *
  * This callback may be called by an internal event thread and as such it is
  * recommended the callback do minimal processing before returning.
  *
  * libusb will call this function later, when a matching event had happened on
  * a matching device. See \ref libusb_hotplug for more information.
  *
  * It is safe to call either libusb_hotplug_register_callback() or
  * libusb_hotplug_deregister_callback() from within a callback function.
  *
  * Since version 1.0.16, \ref LIBUSB_API_VERSION >= 0x01000102
  *
  * \param ctx            context of this notification
  * \param device         libusb_device this event occurred on
  * \param event          event that occurred
  * \param user_data      user data provided when this callback was registered
  * \returns bool whether this callback is finished processing events.
  *  returning 1 will cause this callback to be deregistered
}
  type
    Tlibusb_hotplug_callback_fn = function(ctx: Plibusb_context; device: Plibusb_device; event: Tlibusb_hotplug_event; user_data: Pointer): Integer; cdecl;

{
  * \ingroup libusb_hotplug
  * Register a hotplug callback function
  *
  * Register a callback with the libusb_context. The callback will fire
  * when a matching event occurs on a matching device. The callback is
  * armed until either it is deregistered with libusb_hotplug_deregister_callback()
  * or the supplied callback returns 1 to indicate it is finished processing events.
  *
  * If the \ref LIBUSB_HOTPLUG_ENUMERATE is passed the callback will be
  * called with a \ref LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED for all devices
  * already plugged into the machine. Note that libusb modifies its internal
  * device list from a separate thread, while calling hotplug callbacks from
  * libusb_handle_events(), so it is possible for a device to already be present
  * on, or removed from, its internal device list, while the hotplug callbacks
  * still need to be dispatched. This means that when using \ref
  * LIBUSB_HOTPLUG_ENUMERATE, your callback may be called twice for the arrival
  * of the same device, once from libusb_hotplug_register_callback() and once
  * from libusb_handle_events(); and/or your callback may be called for the
  * removal of a device for which an arrived call was never made.
  *
  * Since version 1.0.16, \ref LIBUSB_API_VERSION >= 0x01000102
  *
  * \param[in] ctx context to register this callback with
  * \param[in] events bitwise or of events that will trigger this callback. See \ref
  *            libusb_hotplug_event
  * \param[in] flags hotplug callback flags. See \ref libusb_hotplug_flag
  * \param[in] vendor_id the vendor id to match or \ref LIBUSB_HOTPLUG_MATCH_ANY
  * \param[in] product_id the product id to match or \ref LIBUSB_HOTPLUG_MATCH_ANY
  * \param[in] dev_class the device class to match or \ref LIBUSB_HOTPLUG_MATCH_ANY
  * \param[in] cb_fn the function to be invoked on a matching event/device
  * \param[in] user_data user data to pass to the callback function
  * \param[out] callback_handle pointer to store the handle of the allocated callback (can be NULL)
  * \returns LIBUSB_SUCCESS on success LIBUSB_ERROR code on failure
}

{ * Definiert in der Funktionstyp definitionen
  Tlibusb_hotplug_register_callback = function(
    ctx: Plibusb_context;
    events: Tlibusb_hotplug_event;
    flags: Tlibusb_hotplug_flag;
    vendor_id: Integer;
    product_id: Integer;
    dev_class: Integer;
    cb_fn: Tlibusb_hotplug_callback_fn;
    user_data: Pointer;
      callback_handle: Tlibusb_hotplug_callback_handle): Integer;
}

{
  * \ingroup libusb_hotplug
  * Deregisters a hotplug callback.
  *
  * Deregister a callback from a libusb_context. This function is safe to call from within
  * a hotplug callback.
  *
  * Since version 1.0.16, \ref LIBUSB_API_VERSION >= 0x01000102
  *
  * \param[in] ctx context this callback is registered with
  * \param[in] callback_handle the handle of the callback to deregister
}
//    Tlibusb_hotplug_deregister_callback = procedure(ctx: Plibusb_context;	callback_handle: Tlibusb_hotplug_callback_handle);


{$define STATIC_LOAD_DISABELD}
{$if defined(STATIC_LOAD_ENABELED)}
//Library initialization/deinitialization
  function libusb_init(out ctx: Plibusb_context): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_exit(ctx: Plibusb_context); stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_set_debug(ctx: Plibusb_context; level: Integer); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_version: Plibusb_version; stdcall; external LIBUSB_1_0_NAME;
  function libusb_has_capability(capability: UInt32): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_error_name(errcode: Integer): PAnsiChar; stdcall; external LIBUSB_1_0_NAME;
  function libusb_setlocale(const locale: PAnsiChar): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_strerror(errcode: Tlibusb_error): PAnsiChar; stdcall; external LIBUSB_1_0_NAME;

//Device handling and enumeration
  function libusb_get_device_list(ctx :Plibusb_context; out list: PPlibusb_device): Tssize; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_device_list(list: Plibusb_device;	unref_devices: Integer); stdcall; external LIBUSB_1_0_NAME;
  function libusb_ref_device(dev: Plibusb_device): Plibusb_device; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_unref_device(dev: Plibusb_device); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_configuration(dev: Plibusb_device_handle; out config: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_device_descriptor(dev: Plibusb_device; out desc: Tlibusb_device_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_active_config_descriptor(dev: Plibusb_device; out config: Plibusb_config_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_config_descriptor(dev: Plibusb_device; config_index: Byte; out config: Plibusb_config_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_config_descriptor_by_value(dev: Plibusb_device; bConfigurationValue: Byte; out confg: Plibusb_config_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_config_descriptor(config: Plibusb_config_descriptor); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_ss_endpoint_companion_descriptor(endpoint: Plibusb_endpoint_descriptor; out ep_comp: Plibusb_ss_endpoint_companion_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;

  procedure libusb_free_ss_endpoint_companion_descriptor(ep_comp: Plibusb_ss_endpoint_companion_descriptor); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_bos_descriptor(dev_handle: Plibusb_device_handle; out bos: Plibusb_bos_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_bos_descriptor(bos: Plibusb_bos_descriptor); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_usb_2_0_extension_descriptor(ctx :Plibusb_context; dev_cap: Plibusb_bos_dev_capability_descriptor; out usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_usb_2_0_extension_descriptor(usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_ss_usb_device_capability_descriptor(ctx: Plibusb_context; dev_cap: Plibusb_bos_dev_capability_descriptor; out Pss_usb_device_cap: Plibusb_ss_usb_device_capability_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_ss_usb_device_capability_descriptor(ss_usb_device_cap: Plibusb_ss_usb_device_capability_descriptor); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_container_id_descriptor(ctx: Plibusb_context; dev_cap: Plibusb_bos_dev_capability_descriptor; out container_id: Plibusb_container_id_descriptor): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_container_id_descriptor(container_id: Plibusb_container_id_descriptor); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_bus_number(dev: Plibusb_device): Byte; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_port_number(dev: Plibusb_device): Byte; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_port_numbers(dev: Plibusb_device; var port_numbers: array of Byte; port_numbers_len: Integer): Integer; stdcall; external LIBUSB_1_0_NAME; deprecated;

  function libusb_get_port_path(ctx: Plibusb_context; dev: Plibusb_device; out path: Byte; path_length: Byte): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_parent(dev: Plibusb_device): Plibusb_device; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_device_address(dev: Plibusb_device): Byte; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_device_speed(dev: Plibusb_device): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_max_packet_size(dev: Plibusb_device; endpoint: Byte): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_max_iso_packet_size(dev: Plibusb_device; endpoint: Byte): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_open(dev: Plibusb_device; out dev_handle: Plibusb_device_handle): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_close(dev_handle: Plibusb_device_handle); stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_device(dev_handle: Plibusb_device_handle): Plibusb_device; stdcall; external LIBUSB_1_0_NAME;
  function libusb_set_configuration(dev_handle: Plibusb_device_handle; configuration: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_claim_interface(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_release_interface(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_open_device_with_vid_pid(ctx: Plibusb_context; vendor_id: Word; product_id: Word): Plibusb_device_handle; stdcall; external LIBUSB_1_0_NAME;
  function libusb_set_interface_alt_setting(dev_handle: Plibusb_device_handle; interface_number: Integer; alternate_setting: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_clear_halt(dev_handle: Plibusb_device_handle;	endpoint: Byte): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_reset_device(dev_handle: Plibusb_device_handle): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_alloc_streams(dev_handle: Plibusb_device_handle; num_streams: LongWord; out endpoints: Byte; num_endpoints: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_free_streams(dev_handle: Plibusb_device_handle; var endpoints: Byte; num_endpoints: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_dev_mem_alloc(dev_handle: Plibusb_device_handle; length: size_t): PByte; stdcall; external LIBUSB_1_0_NAME;
  function libusb_dev_mem_free(dev_handle: Plibusb_device_handle; var buffer: Byte; length:size_t): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_kernel_driver_active(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_detach_kernel_driver(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_attach_kernel_driver(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_set_auto_detach_kernel_driver(dev_handle: Plibusb_device_handle; enable: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;

  function libusb_alloc_transfer(iso_packets: Integer): Plibusb_transfer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_submit_transfer(transfer: Plibusb_transfer): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_cancel_transfer(transfer: Plibusb_transfer): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_free_transfer(transfer: Plibusb_transfer); stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_transfer_set_stream_id(transfer: Plibusb_transfer; stream_id: LongWord); stdcall; external LIBUSB_1_0_NAME;
  function libusb_transfer_get_stream_id(transfer: Plibusb_transfer): LongWord; stdcall; external LIBUSB_1_0_NAME;

// Hotplug
  function libusb_hotplug_register_callback(ctx: Plibusb_context; events: Tlibusb_hotplug_events; flags: Tlibusb_hotplug_flag;	vendor_id: Integer;
      product_id: Integer; dev_class: Integer; cb_fn: Tlibusb_hotplug_callback_fn; user_data: Pointer;
      callback_handle: Tlibusb_hotplug_callback_handle): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_hotplug_deregister_callback(ctx: Plibusb_context; callback_handle: Tlibusb_hotplug_callback_handle); stdcall; external LIBUSB_1_0_NAME;

// sync I/O
  function libusb_control_transfer(dev_handle: Plibusb_device_handle; request_type: Byte; bRequest: Byte; wValue: Word; wIndex: Word; data: PByte; wLength: Word; timeout: LongWord): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_bulk_transfer(dev_handle: Plibusb_device_handle; endpoint: Byte; data: PByte; length: Integer; out actual_length: Integer; timeout: LongWord): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_interrupt_transfer(dev_handle: Plibusb_device_handle; endpoint: Byte; data: PByte; length: Integer; out actual_length: Integer; timeout: Longword): Integer; stdcall; external LIBUSB_1_0_NAME;

  function libusb_get_string_descriptor_ascii(dev_handle: Plibusb_device_handle; desc_index: Byte; data: PByte; length: Integer): Integer; stdcall; external LIBUSB_1_0_NAME;

// polling and timeouts
  function libusb_try_lock_events(ctx: Plibusb_context): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_lock_events(ctx: Plibusb_context); stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_unlock_events(ctx: Plibusb_context); stdcall; external LIBUSB_1_0_NAME;
  function libusb_event_handling_ok(ctx: Plibusb_context): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_event_handler_active(ctx: Plibusb_context): Integer; stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_interrupt_event_handler(ctx: Plibusb_context); stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_lock_event_waiters(ctx: Plibusb_context); stdcall; external LIBUSB_1_0_NAME;
  procedure libusb_unlock_event_waiters(ctx: Plibusb_context); stdcall; external LIBUSB_1_0_NAME;
  function libusb_wait_for_event(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_handle_events_timeout(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_handle_events_timeout_completed(ctx: Plibusb_context; tv: Plibusb_timeval; completed: PInteger): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_handle_events(ctx: Plibusb_context): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_handle_events_completed(ctx: Plibusb_context; completed: PInteger): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_handle_events_locked(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_pollfds_handle_timeouts(ctx: Plibusb_context): Integer; stdcall; external LIBUSB_1_0_NAME;
  function libusb_get_next_timeout(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; stdcall; external LIBUSB_1_0_NAME;

//  function libusb_set_option(ctx: Plibusb_context; option: Tlibusb_option; args: array of const): Integer;  stdcall; external LIBUSB_1_0_NAME; deprecated;
  {$endif}

{$define DYNAMIC_LOAD_ENABELD}
{$if defined(DYNAMIC_LOAD_ENABELD)}
type
  TFlibusb_init = function(out ctx: Plibusb_context): Integer; cdecl;
  TFlibusb_exit = procedure(ctx: Plibusb_context); cdecl;
  TFlibusb_set_debug = procedure(ctx: Plibusb_context; level: Integer); cdecl;
  TFlibusb_get_version = function: Plibusb_version; cdecl;
  TFlibusb_has_capability = function(capability: UInt32): Integer; cdecl;
  TFlibusb_error_name = function(errcode: Integer): PAnsiChar; cdecl;
  TFlibusb_setlocale = function(const locale: PAnsiChar): Integer; cdecl;
  TFlibusb_strerror = function(errcode: Tlibusb_error): PAnsiChar; cdecl;
  TFlibusb_get_device_list = function(ctx :Plibusb_context; out list: PPlibusb_device): Tssize; cdecl;
  TFlibusb_free_device_list = procedure(list: PPlibusb_device;	unref_devices: Integer); cdecl;
  TFlibusb_ref_device = function(dev: Plibusb_device): Plibusb_device; cdecl;
  TFlibusb_unref_device = procedure(dev: Plibusb_device); cdecl;
  TFlibusb_get_configuration = function(dev: Plibusb_device_handle; out config: Integer): Integer; cdecl;
  TFlibusb_get_device_descriptor = function(dev: Plibusb_device; out desc: Tlibusb_device_descriptor): Integer; cdecl;
  TFlibusb_get_active_config_descriptor = function(dev: Plibusb_device;	out config: Plibusb_config_descriptor): Integer; cdecl;
  TFlibusb_get_config_descriptor = function(dev: Plibusb_device; config_index: Byte; out config: Plibusb_config_descriptor): Integer; cdecl;
  TFlibusb_get_config_descriptor_by_value = function(dev: Plibusb_device; bConfigurationValue: Byte; out confg: Plibusb_config_descriptor): Integer; cdecl;
  TFlibusb_free_config_descriptor = procedure(config: Plibusb_config_descriptor); cdecl;
  TFlibusb_get_ss_endpoint_companion_descriptor = function(ctx :Plibusb_context; endpoint: Plibusb_endpoint_descriptor;	out ep_comp: Plibusb_ss_endpoint_companion_descriptor): Integer; cdecl;
  TFlibusb_free_ss_endpoint_companion_descriptor = procedure(ep_comp: Plibusb_ss_endpoint_companion_descriptor); cdecl;
  TFlibusb_get_bos_descriptor = function(dev_handle: Plibusb_device_handle; out bos: Plibusb_bos_descriptor): Integer; cdecl;
  TFlibusb_free_bos_descriptor = procedure(bos: Plibusb_bos_descriptor); cdecl;
  TFlibusb_get_usb_2_0_extension_descriptor = function(ctx :Plibusb_context; dev_cap: Plibusb_bos_dev_capability_descriptor; out usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor): Integer; cdecl;
  TFlibusb_free_usb_2_0_extension_descriptor = procedure(usb_2_0_extension: Plibusb_usb_2_0_extension_descriptor); cdecl;
  TFlibusb_get_ss_usb_device_capability_descriptor = function(ctx: Plibusb_context; dev_cap: Plibusb_bos_dev_capability_descriptor; out Pss_usb_device_cap: Plibusb_ss_usb_device_capability_descriptor): Integer; cdecl;
  TFlibusb_free_ss_usb_device_capability_descriptor = procedure(ss_usb_device_cap: Plibusb_ss_usb_device_capability_descriptor); cdecl;
  TFlibusb_get_container_id_descriptor = function(ctx: Plibusb_context; dev_cap: Plibusb_bos_dev_capability_descriptor; out container_id: Plibusb_container_id_descriptor): Integer; cdecl;
  TFlibusb_free_container_id_descriptor = procedure(container_id: Plibusb_container_id_descriptor); cdecl;
  TFlibusb_get_bus_number = function(dev: Plibusb_device): Byte; cdecl;
  TFlibusb_get_port_number = function(dev: Plibusb_device): Byte; cdecl;
  TFlibusb_get_port_numbers = function(dev: Plibusb_device; var port_numbers:array of Byte; port_numbers_len: Integer): Integer; cdecl;
  TFlibusb_get_port_path = function(ctx: Plibusb_context; dev: Plibusb_device; out path: Byte; path_length: Byte): Integer; cdecl;
  TFlibusb_get_parent = function(dev: Plibusb_device): Plibusb_device; cdecl;
  TFlibusb_get_device_address = function(dev: Plibusb_device): Byte; cdecl;
  TFlibusb_get_device_speed = function(dev: Plibusb_device): Integer; cdecl;
  TFlibusb_get_max_packet_size = function(dev: Plibusb_device; endpoint: Byte): Integer; cdecl;
  TFlibusb_get_max_iso_packet_size = function(dev: Plibusb_device; endpoint: Byte): Integer; cdecl;
  TFlibusb_open = function(dev: Plibusb_device; out dev_handle: Plibusb_device_handle): Integer; cdecl;
  TFlibusb_close = procedure(dev_handle: Plibusb_device_handle); cdecl;
  TFlibusb_get_device = function(dev_handle: Plibusb_device_handle): Plibusb_device; cdecl;
  TFlibusb_set_configuration = function(dev_handle: Plibusb_device_handle; configuration: Integer): Integer; cdecl;
  TFlibusb_claim_interface = function(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; cdecl;
  TFlibusb_release_interface = function(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; cdecl;
  TFlibusb_open_device_with_vid_pid = function(ctx: Plibusb_context; vendor_id: Word; product_id: Word): Plibusb_device_handle; cdecl;
  TFlibusb_set_interface_alt_setting = function(dev_handle: Plibusb_device_handle; interface_number: Integer; alternate_setting: Integer): Integer; cdecl;
  TFlibusb_clear_halt = function(dev_handle: Plibusb_device_handle;	endpoint: Byte): Integer; cdecl;
  TFlibusb_reset_device = function(dev_handle: Plibusb_device_handle): Integer; cdecl;
  TFlibusb_alloc_streams = function(dev_handle: Plibusb_device_handle; num_streams: LongWord; out endpoints: Byte; num_endpoints: Integer): Integer; cdecl;
  TFlibusb_free_streams = function(dev_handle: Plibusb_device_handle; var endpoints: Byte; num_endpoints: Integer): Integer; cdecl;
  TFlibusb_dev_mem_alloc = function(dev_handle: Plibusb_device_handle; length: size_t): PByte; cdecl;
  TFlibusb_dev_mem_free = function(dev_handle: Plibusb_device_handle; var buffer: Byte; length:size_t): Integer; cdecl;
  TFlibusb_kernel_driver_active = function(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; cdecl;
  TFlibusb_detach_kernel_driver = function(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; cdecl;
  TFlibusb_attach_kernel_driver = function(dev_handle: Plibusb_device_handle; interface_number: Integer): Integer; cdecl;
  TFlibusb_set_auto_detach_kernel_driver = function(dev_handle: Plibusb_device_handle; enable: Integer): Integer; cdecl;
  TFlibusb_alloc_transfer = function(iso_packets: Integer): Plibusb_transfer; cdecl;
  TFlibusb_submit_transfer = function(transfer: Plibusb_transfer): Integer; cdecl;
  TFlibusb_cancel_transfer = function(transfer: Plibusb_transfer): Integer; cdecl;
  TFlibusb_free_transfer = procedure(transfer: Plibusb_transfer); cdecl;
  TFlibusb_transfer_set_stream_id = procedure(transfer: Plibusb_transfer; stream_id: LongWord); cdecl;
  TFlibusb_transfer_get_stream_id = function(transfer: Plibusb_transfer): LongWord; cdecl;
  TFlibusb_control_transfer = function(dev_handle: Plibusb_device_handle; request_type: Byte; bRequest: Tlibusb_standard_request; wValue: Word; wIndex: Word; data: PByte; wLength: Word; timeout: LongWord): Integer; cdecl;
//  TFlibusb_control_transfer = function(dev_handle: Plibusb_device_handle; request_type: Byte; bRequest: Byte; wValue: Word; wIndex: Word; data: PByte; wLength: Word; timeout: LongWord): Integer; cdecl;
  TFlibusb_sync_io_transfer = function(dev_handle: Plibusb_device_handle; endpoint: Byte; data: PByte; length: Integer; out actual_length: Integer; timeout: LongWord): Integer; cdecl;
  TFlibusb_bulk_transfer = TFlibusb_sync_io_transfer;
  TFlibusb_interrupt_transfer = TFlibusb_sync_io_transfer;
  //TFlibusb_bulk_transfer =      function(dev_handle: Plibusb_device_handle; endpoint: Byte; data: PByte; length: Integer; out actual_length: Integer; timeout: LongWord): Integer; cdecl;
  //TFlibusb_interrupt_transfer = function(dev_handle: Plibusb_device_handle; endpoint: Byte; data: PByte; length: Integer; out actual_length: Integer; timeout: Longword): Integer; cdecl;
  TFlibusb_get_string_descriptor_ascii = function(dev_handle: Plibusb_device_handle; desc_index: Byte; data: PByte; length: Integer): Integer; cdecl;
  TFlibusb_try_lock_events = function(ctx: Plibusb_context): Integer; cdecl;
  TFlibusb_lock_events = procedure(ctx: Plibusb_context); cdecl;
  TFlibusb_unlock_events = procedure(ctx: Plibusb_context); cdecl;
  TFlibusb_event_handling_ok = function(ctx: Plibusb_context): Integer; cdecl;
  TFlibusb_event_handler_active = function(ctx: Plibusb_context): Integer; cdecl;
  TFlibusb_interrupt_event_handler = procedure(ctx: Plibusb_context); cdecl;
  TFlibusb_lock_event_waiters = procedure(ctx: Plibusb_context); cdecl;
  TFlibusb_unlock_event_waiters = procedure(ctx: Plibusb_context); cdecl;
  TFlibusb_wait_for_event = function(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; cdecl;
  TFlibusb_handle_events_timeout = function(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; cdecl;
  TFlibusb_handle_events_timeout_completed = function(ctx: Plibusb_context; tv: Plibusb_timeval; completed: PInteger): Integer; cdecl;
  TFlibusb_handle_events = function(ctx: Plibusb_context): Integer; cdecl;
  TFlibusb_handle_events_completed = function(ctx: Plibusb_context; completed: PInteger): Integer; cdecl;
  TFlibusb_handle_events_locked = function(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; cdecl;
  TFlibusb_pollfds_handle_timeouts = function(ctx: Plibusb_context): Integer; cdecl;
  TFlibusb_get_next_timeout = function(ctx: Plibusb_context; tv: Plibusb_timeval): Integer; cdecl;
  Tlibusb_hotplug_register_callback = function(ctx: Plibusb_context; events: Tlibusb_hotplug_events;
      flags: Tlibusb_hotplug_flag; vendor_id: Integer; product_id: Integer; dev_class: Integer;
      cb_fn: Tlibusb_hotplug_callback_fn; user_data: Pointer; var callback_handle: Tlibusb_hotplug_callback_handle): Integer; cdecl;
  Tlibusb_hotplug_deregister_callback = procedure(ctx: Plibusb_context;	callback_handle: Tlibusb_hotplug_callback_handle);  cdecl;
//  TFlibusb_set_option = function(ctx: Plibusb_context; option: Tlibusb_option; args: array of const): Integer; cdecl; deprecated;


function LibusbLibaryLoad: Boolean;
procedure LibusbLibaryFree;
function LibusbLibaryIsLoaded: Boolean;

var
  libusb_init: TFlibusb_init;
  libusb_exit: TFlibusb_exit;
  libusb_set_debug: TFlibusb_set_debug;
  libusb_get_version: TFlibusb_get_version;
  libusb_has_capability: TFlibusb_has_capability;
  libusb_error_name: TFlibusb_error_name;
  libusb_setlocale: TFlibusb_setlocale;
  libusb_strerror: TFlibusb_strerror;
  libusb_get_device_list: TFlibusb_get_device_list;
  libusb_free_device_list: TFlibusb_free_device_list;
  libusb_ref_device: TFlibusb_ref_device;
  libusb_unref_device: TFlibusb_unref_device;
  libusb_get_configuration: TFlibusb_get_configuration;
  libusb_get_device_descriptor: TFlibusb_get_device_descriptor;
  libusb_get_active_config_descriptor: TFlibusb_get_active_config_descriptor;
  libusb_get_config_descriptor: TFlibusb_get_config_descriptor;
  libusb_get_config_descriptor_by_value: TFlibusb_get_config_descriptor_by_value;
  libusb_free_config_descriptor: TFlibusb_free_config_descriptor;
  libusb_get_ss_endpoint_companion_descriptor: TFlibusb_get_ss_endpoint_companion_descriptor;
  libusb_free_ss_endpoint_companion_descriptor: TFlibusb_free_ss_endpoint_companion_descriptor;
  libusb_get_bos_descriptor: TFlibusb_get_bos_descriptor;
  libusb_free_bos_descriptor: TFlibusb_free_bos_descriptor;
  libusb_get_usb_2_0_extension_descriptor: TFlibusb_get_usb_2_0_extension_descriptor;
  libusb_free_usb_2_0_extension_descriptor: TFlibusb_free_usb_2_0_extension_descriptor;
  libusb_get_ss_usb_device_capability_descriptor: TFlibusb_get_ss_usb_device_capability_descriptor;
  libusb_free_ss_usb_device_capability_descriptor: TFlibusb_free_ss_usb_device_capability_descriptor;
  libusb_get_container_id_descriptor: TFlibusb_get_container_id_descriptor;
  libusb_free_container_id_descriptor: TFlibusb_free_container_id_descriptor;
  libusb_get_bus_number: TFlibusb_get_bus_number;
  libusb_get_port_number: TFlibusb_get_port_number;
  libusb_get_port_numbers: TFlibusb_get_port_numbers;
  libusb_get_port_path: TFlibusb_get_port_path;
  libusb_get_parent: TFlibusb_get_parent;
  libusb_get_device_address: TFlibusb_get_device_address;
  libusb_get_device_speed: TFlibusb_get_device_speed;
  libusb_get_max_packet_size: TFlibusb_get_max_packet_size;
  libusb_get_max_iso_packet_size: TFlibusb_get_max_iso_packet_size;
  libusb_open: TFlibusb_open;
  libusb_close: TFlibusb_close;
  libusb_get_device: TFlibusb_get_device;
  libusb_set_configuration: TFlibusb_set_configuration;
  libusb_claim_interface: TFlibusb_claim_interface;
  libusb_release_interface: TFlibusb_release_interface;
  libusb_open_device_with_vid_pid: TFlibusb_open_device_with_vid_pid;
  libusb_set_interface_alt_setting: TFlibusb_set_interface_alt_setting;
  libusb_clear_halt: TFlibusb_clear_halt;
  libusb_reset_device: TFlibusb_reset_device;
  libusb_alloc_streams: TFlibusb_alloc_streams;
  libusb_free_streams: TFlibusb_free_streams;
  libusb_dev_mem_alloc: TFlibusb_dev_mem_alloc;
  libusb_dev_mem_free: TFlibusb_dev_mem_free;
  libusb_kernel_driver_active: TFlibusb_kernel_driver_active;
  libusb_detach_kernel_driver: TFlibusb_detach_kernel_driver;
  libusb_attach_kernel_driver: TFlibusb_attach_kernel_driver;
  libusb_set_auto_detach_kernel_driver: TFlibusb_set_auto_detach_kernel_driver;
  libusb_alloc_transfer: TFlibusb_alloc_transfer;
  libusb_submit_transfer: TFlibusb_submit_transfer;
  libusb_cancel_transfer: TFlibusb_cancel_transfer;
  libusb_free_transfer: TFlibusb_free_transfer;
  libusb_transfer_set_stream_id: TFlibusb_transfer_set_stream_id;
  libusb_transfer_get_stream_id: TFlibusb_transfer_get_stream_id;
  libusb_control_transfer: TFlibusb_control_transfer;
  libusb_bulk_transfer: TFlibusb_bulk_transfer;
  libusb_interrupt_transfer: TFlibusb_interrupt_transfer;
  libusb_get_string_descriptor_ascii: TFlibusb_get_string_descriptor_ascii;
  libusb_try_lock_events: TFlibusb_try_lock_events;
  libusb_lock_events: TFlibusb_lock_events;
  libusb_unlock_events: TFlibusb_unlock_events;
  libusb_event_handling_ok: TFlibusb_event_handling_ok;
  libusb_event_handler_active: TFlibusb_event_handler_active;
  libusb_interrupt_event_handler: TFlibusb_interrupt_event_handler;
  libusb_lock_event_waiters: TFlibusb_lock_event_waiters;
  libusb_unlock_event_waiters: TFlibusb_unlock_event_waiters;
  libusb_wait_for_event: TFlibusb_wait_for_event;
  libusb_handle_events_timeout: TFlibusb_handle_events_timeout;
  libusb_handle_events_timeout_completed: TFlibusb_handle_events_timeout_completed;
  libusb_handle_events: TFlibusb_handle_events;
  libusb_handle_events_completed: TFlibusb_handle_events_completed;
  libusb_handle_events_locked: TFlibusb_handle_events_locked;
  libusb_pollfds_handle_timeouts: TFlibusb_pollfds_handle_timeouts;
  libusb_get_next_timeout: TFlibusb_get_next_timeout;
  libusb_hotplug_register_callback: Tlibusb_hotplug_register_callback;
  libusb_hotplug_deregister_callback: Tlibusb_hotplug_deregister_callback;

//  libusb_set_option: TFlibusb_set_option; deprecated;;
{$endif} //{$if defined(DYNAMIC_LOAD_ENABELD)}

{
  *
  * \ingroup libusb_misc
  * Convert a 16-bit value from host-endian to little-endian format. On
  * little endian systems, this function does nothing. On big endian systems,
  * the bytes are swapped.
  * \param x the host-endian value to convert
  * \returns the value in little-endian byte order
}

  function libusb_cpu_to_le16(const Value: Word): Word;
  function libusb_le16_to_cpu(const Value: Word): Word;
  function libusb_get_descriptor(dev_handle: Plibusb_device_handle; desc_type: Byte; desc_index: Byte; data: PByte; length: Integer): Integer;
  function libusb_get_string_descriptor(dev_handle: Plibusb_device_handle; desc_index: Byte; langid: Word; data: PByte; length: Integer): Integer;
  procedure libusb_lock;
  procedure libusb_unlock;


implementation

{$define DYNAMIC_LOAD_ENABELD}
{$if defined(DYNAMIC_LOAD_ENABELD)}
uses
  SyncObjs,
  dynlibs;

var
  libusb_loaded_ref_count: Integer = 0;

function LibusbLibaryIsLoaded: Boolean;
begin
  Result:= libusb_loaded_ref_count > 0;
end;

var
  cs: TCriticalSection;
  libusb_libary_handle: TLibHandle = dynlibs.NilHandle;

procedure libusb_lock;
begin
  if cs = nil then begin
    cs:= TCriticalSection.Create;
  end;
  cs.Enter;
end;

procedure libusb_unlock;
var
  tmp: TCriticalSection;
begin
  if Assigned(cs) then begin
    if libusb_libary_handle <> dynlibs.NilHandle then begin
      cs.Release;
    end else begin
      tmp:= cs;
      cs:= nil;
      tmp.Release;
      tmp.Free;
    end;
  end;
end;

function LibusbLibaryLoad: Boolean;

  function GetProcedure(const ProcedureName: String): Pointer;
  begin
    Result:= GetProcedureAddress(libusb_libary_handle, ProcedureName);
    if Result = nil then begin
      raise ELibUsb.CreateFmt('LibusbLibaryLoad:: Procedure "%s" not found!', [ProcedureName]);
    end;
  end;

begin
  try
    libusb_lock;
    if libusb_loaded_ref_count > 0 then begin
      inc(libusb_loaded_ref_count);
      Result:= True;
      exit;
    end else begin
      Result:= False;
    end;

    libusb_libary_handle:= LoadLibrary(LIBUSB_1_0_NAME);
    if libusb_libary_handle = dynlibs.NilHandle then begin
      raise ELibUsb.CreateFmt('LibusbLibaryLoad:: Libary "%s", not loaded!', [LIBUSB_1_0_NAME]);
    end;
    libusb_init:= TFlibusb_init(GetProcedure('libusb_init'));
    libusb_exit:= TFlibusb_exit(GetProcedure('libusb_exit'));
    libusb_set_debug:= TFlibusb_set_debug(GetProcedure('libusb_set_debug'));
    libusb_get_version:= TFlibusb_get_version(GetProcedure('libusb_get_version'));
    libusb_has_capability:= TFlibusb_has_capability(GetProcedure('libusb_has_capability'));
    libusb_error_name:= TFlibusb_error_name(GetProcedure('libusb_error_name'));
    libusb_setlocale:= TFlibusb_setlocale(GetProcedure('libusb_setlocale'));
    libusb_strerror:= TFlibusb_strerror(GetProcedure('libusb_strerror'));
    libusb_get_device_list:= TFlibusb_get_device_list(GetProcedure('libusb_get_device_list'));
    libusb_free_device_list:= TFlibusb_free_device_list(GetProcedure('libusb_free_device_list'));
    libusb_ref_device:= TFlibusb_ref_device(GetProcedure('libusb_ref_device'));
    libusb_unref_device:= TFlibusb_unref_device(GetProcedure('libusb_unref_device'));
    libusb_get_configuration:= TFlibusb_get_configuration(GetProcedure('libusb_get_configuration'));
    libusb_get_device_descriptor:= TFlibusb_get_device_descriptor(GetProcedure('libusb_get_device_descriptor'));
    libusb_get_active_config_descriptor:= TFlibusb_get_active_config_descriptor(GetProcedure('libusb_get_active_config_descriptor'));
    libusb_get_config_descriptor:= TFlibusb_get_config_descriptor(GetProcedure('libusb_get_config_descriptor'));
    libusb_get_config_descriptor_by_value:= TFlibusb_get_config_descriptor_by_value(GetProcedure('libusb_get_config_descriptor_by_value'));
    libusb_free_config_descriptor:= TFlibusb_free_config_descriptor(GetProcedure('libusb_free_config_descriptor'));
    libusb_get_ss_endpoint_companion_descriptor:= TFlibusb_get_ss_endpoint_companion_descriptor(GetProcedure('libusb_get_ss_endpoint_companion_descriptor'));
    libusb_free_ss_endpoint_companion_descriptor:= TFlibusb_free_ss_endpoint_companion_descriptor(GetProcedure('libusb_free_ss_endpoint_companion_descriptor'));
    libusb_get_bos_descriptor:= TFlibusb_get_bos_descriptor(GetProcedure('libusb_get_bos_descriptor'));
    libusb_free_bos_descriptor:= TFlibusb_free_bos_descriptor(GetProcedure('libusb_free_bos_descriptor'));
    libusb_get_usb_2_0_extension_descriptor:= TFlibusb_get_usb_2_0_extension_descriptor(GetProcedure('libusb_get_usb_2_0_extension_descriptor'));
    libusb_free_usb_2_0_extension_descriptor:= TFlibusb_free_usb_2_0_extension_descriptor(GetProcedure('libusb_free_usb_2_0_extension_descriptor'));
    libusb_get_ss_usb_device_capability_descriptor:= TFlibusb_get_ss_usb_device_capability_descriptor(GetProcedure('libusb_get_ss_usb_device_capability_descriptor'));
    libusb_free_ss_usb_device_capability_descriptor:= TFlibusb_free_ss_usb_device_capability_descriptor(GetProcedure('libusb_free_ss_usb_device_capability_descriptor'));
    libusb_get_container_id_descriptor:= TFlibusb_get_container_id_descriptor(GetProcedure('libusb_get_container_id_descriptor'));
    libusb_free_container_id_descriptor:= TFlibusb_free_container_id_descriptor(GetProcedure('libusb_free_container_id_descriptor'));
    libusb_get_bus_number:= TFlibusb_get_bus_number(GetProcedure('libusb_get_bus_number'));
    libusb_get_port_number:= TFlibusb_get_port_number(GetProcedure('libusb_get_port_number'));
    libusb_get_port_numbers:= TFlibusb_get_port_numbers(GetProcedure('libusb_get_port_numbers'));
    libusb_get_port_path:= TFlibusb_get_port_path(GetProcedure('libusb_get_port_path'));
    libusb_get_parent:= TFlibusb_get_parent(GetProcedure('libusb_get_parent'));
    libusb_get_device_address:= TFlibusb_get_device_address(GetProcedure('libusb_get_device_address'));
    libusb_get_device_speed:= TFlibusb_get_device_speed(GetProcedure('libusb_get_device_speed'));
    libusb_get_max_packet_size:= TFlibusb_get_max_packet_size(GetProcedure('libusb_get_max_packet_size'));
    libusb_get_max_iso_packet_size:= TFlibusb_get_max_iso_packet_size(GetProcedure('libusb_get_max_iso_packet_size'));
    libusb_open:= TFlibusb_open(GetProcedure('libusb_open'));
    libusb_close:= TFlibusb_close(GetProcedure('libusb_close'));
    libusb_get_device:= TFlibusb_get_device(GetProcedure('libusb_get_device'));
    libusb_set_configuration:= TFlibusb_set_configuration(GetProcedure('libusb_set_configuration'));
    libusb_claim_interface:= TFlibusb_claim_interface(GetProcedure('libusb_claim_interface'));
    libusb_release_interface:= TFlibusb_release_interface(GetProcedure('libusb_release_interface'));
    libusb_open_device_with_vid_pid:= TFlibusb_open_device_with_vid_pid(GetProcedure('libusb_open_device_with_vid_pid'));
    libusb_set_interface_alt_setting:= TFlibusb_set_interface_alt_setting(GetProcedure('libusb_set_interface_alt_setting'));
    libusb_clear_halt:= TFlibusb_clear_halt(GetProcedure('libusb_clear_halt'));
    libusb_reset_device:= TFlibusb_reset_device(GetProcedure('libusb_reset_device'));
    libusb_alloc_streams:= TFlibusb_alloc_streams(GetProcedure('libusb_alloc_streams'));
    libusb_free_streams:= TFlibusb_free_streams(GetProcedure('libusb_free_streams'));
    libusb_dev_mem_alloc:= TFlibusb_dev_mem_alloc(GetProcedure('libusb_dev_mem_alloc'));
    libusb_dev_mem_free:= TFlibusb_dev_mem_free(GetProcedure('libusb_dev_mem_free'));
    libusb_kernel_driver_active:= TFlibusb_kernel_driver_active(GetProcedure('libusb_kernel_driver_active'));
    libusb_detach_kernel_driver:= TFlibusb_detach_kernel_driver(GetProcedure('libusb_detach_kernel_driver'));
    libusb_attach_kernel_driver:= TFlibusb_attach_kernel_driver(GetProcedure('libusb_attach_kernel_driver'));
    libusb_set_auto_detach_kernel_driver:= TFlibusb_set_auto_detach_kernel_driver(GetProcedure('libusb_set_auto_detach_kernel_driver'));
    libusb_alloc_transfer:= TFlibusb_alloc_transfer(GetProcedure('libusb_alloc_transfer'));
    libusb_submit_transfer:= TFlibusb_submit_transfer(GetProcedure('libusb_submit_transfer'));
    libusb_cancel_transfer:= TFlibusb_cancel_transfer(GetProcedure('libusb_cancel_transfer'));
    libusb_free_transfer:= TFlibusb_free_transfer(GetProcedure('libusb_free_transfer'));
    libusb_transfer_set_stream_id:= TFlibusb_transfer_set_stream_id(GetProcedure('libusb_transfer_set_stream_id'));
    libusb_transfer_get_stream_id:= TFlibusb_transfer_get_stream_id(GetProcedure('libusb_transfer_get_stream_id'));
    libusb_control_transfer:= TFlibusb_control_transfer(GetProcedure('libusb_control_transfer'));
    libusb_bulk_transfer:= TFlibusb_bulk_transfer(GetProcedure('libusb_bulk_transfer'));
    libusb_interrupt_transfer:= TFlibusb_interrupt_transfer(GetProcedure('libusb_interrupt_transfer'));
    libusb_get_string_descriptor_ascii:= TFlibusb_get_string_descriptor_ascii(GetProcedure('libusb_get_string_descriptor_ascii'));
    libusb_try_lock_events:= TFlibusb_try_lock_events(GetProcedure('libusb_try_lock_events'));
    libusb_lock_events:= TFlibusb_lock_events(GetProcedure('libusb_lock_events'));
    libusb_unlock_events:= TFlibusb_unlock_events(GetProcedure('libusb_unlock_events'));
    libusb_event_handling_ok:= TFlibusb_event_handling_ok(GetProcedure('libusb_event_handling_ok'));
    libusb_event_handler_active:= TFlibusb_event_handler_active(GetProcedure('libusb_event_handler_active'));
    libusb_interrupt_event_handler:= TFlibusb_interrupt_event_handler(GetProcedure('libusb_interrupt_event_handler'));
    libusb_lock_event_waiters:= TFlibusb_lock_event_waiters(GetProcedure('libusb_lock_event_waiters'));
    libusb_unlock_event_waiters:= TFlibusb_unlock_event_waiters(GetProcedure('libusb_unlock_event_waiters'));
    libusb_wait_for_event:= TFlibusb_wait_for_event(GetProcedure('libusb_wait_for_event'));
    libusb_handle_events_timeout:= TFlibusb_handle_events_timeout(GetProcedure('libusb_handle_events_timeout'));
    libusb_handle_events_timeout_completed:= TFlibusb_handle_events_timeout_completed(GetProcedure('libusb_handle_events_timeout_completed'));
    libusb_handle_events:= TFlibusb_handle_events(GetProcedure('libusb_handle_events'));
    libusb_handle_events_completed:= TFlibusb_handle_events_completed(GetProcedure('libusb_handle_events_completed'));
    libusb_handle_events_locked:= TFlibusb_handle_events_locked(GetProcedure('libusb_handle_events_locked'));
    libusb_pollfds_handle_timeouts:= TFlibusb_pollfds_handle_timeouts(GetProcedure('libusb_pollfds_handle_timeouts'));
    libusb_get_next_timeout:= TFlibusb_get_next_timeout(GetProcedure('libusb_get_next_timeout'));
    libusb_hotplug_register_callback:= Tlibusb_hotplug_register_callback(GetProcedure('libusb_hotplug_register_callback'));
    libusb_hotplug_deregister_callback:= Tlibusb_hotplug_deregister_callback(GetProcedure('libusb_hotplug_deregister_callback'));

  //  libusb_set_option:= TFlibusb_set_option(GetProcedure('libusb_set_option')); deprecated;
    Result:= True;
    libusb_loaded_ref_count:= 1;
  finally
    libusb_unlock;
  end;
end;

procedure LibusbLibaryFree;
begin
  try
    libusb_lock;
    if libusb_loaded_ref_count > 0 then begin
      dec(libusb_loaded_ref_count);
      if (libusb_loaded_ref_count = 0) and (libusb_libary_handle <> dynlibs.NilHandle) then begin
        FreeLibrary(libusb_libary_handle);
        libusb_libary_handle:= dynlibs.NilHandle;
      end;
    end;
  finally
    libusb_unlock;
  end;
end;

{$endif} //{$if defined(DYNAMIC_LOAD_ENABELD)}


{$IFDEF ENDIAN_LITTLE}
  function libusb_cpu_to_le16(const Value: Word): Word;
  begin
    Result:= Value;
  end;

  function libusb_le16_to_cpu(const Value: Word): Word;
  begin
    Result:= Value;
  end;
{$ENDIF}

{$IFDEF ENDIAN_BIG}
function libusb_cpu_to_le16(const Value: Word): Word;
  begin
    Result:= swap(Value);
  end;

  function libusb_le16_to_cpu(const Value: Word): Word;
  begin
    Result:= swap(Value);
  end;
{$ENDIF}



{ async I/O  }
{
  * \ingroup libusb_asyncio
  * Get the data section of a control transfer. This convenience function is here
  * to remind you that the data does not start until 8 bytes into the actual
  * buffer, as the setup packet comes first.
  *
  * Calling this function only makes sense from a transfer callback function,
  * or situations where you have already allocated a suitably sized buffer at
  * transfer->buffer.
  *
  * \param transfer a transfer
  * \returns pointer to the first byte of the data section
}
function libusb_control_transfer_get_data(transfer: Plibusb_transfer): PByte;
begin
  Result:= transfer^.buffer + LIBUSB_CONTROL_SETUP_SIZE;
end;

{
  * \ingroup libusb_asyncio
  * Get the control setup packet of a control transfer. This convenience
  * function is here to remind you that the control setup occupies the first
  * 8 bytes of the transfer data buffer.
  *
  * Calling this function only makes sense from a transfer callback function,
  * or situations where you have already allocated a suitably sized buffer at
  * transfer->buffer.
  *
  * \param transfer a transfer
  * \returns a casted pointer to the start of the transfer data buffer
}
function libusb_control_transfer_get_setup(transfer: Plibusb_transfer): Plibusb_control_setup;
begin
  Result:= Plibusb_control_setup(transfer^.buffer);
end;

{
  * \ingroup libusb_asyncio
  * Helper function to populate the setup packet (first 8 bytes of the data
  * buffer) for a control transfer. The wIndex, wValue and wLength values should
  * be given in host-endian byte order.
  *
  * \param buffer buffer to output the setup packet into
  * This pointer must be aligned to at least 2 bytes boundary.
  * \param bmRequestType see the
  * \ref libusb_control_setup::bmRequestType "bmRequestType" field of
  * \ref libusb_control_setup
  * \param bRequest see the
  * \ref libusb_control_setup::bRequest "bRequest" field of
  * \ref libusb_control_setup
  * \param wValue see the
  * \ref libusb_control_setup::wValue "wValue" field of
  * \ref libusb_control_setup
  * \param wIndex see the
  * \ref libusb_control_setup::wIndex "wIndex" field of
  * \ref libusb_control_setup
  * \param wLength see the
  * \ref libusb_control_setup::wLength "wLength" field of
  * \ref libusb_control_setup
}

procedure libusb_fill_control_setup(buffer: PByte; bmRequestType: Byte; bRequest: Byte; wValue: Word; wIndex: Word; wLength: Word);
var
  setup: Plibusb_control_setup;
begin
  setup:= Plibusb_control_setup(buffer);
  setup^.bmRequestType:= bmRequestType;
  setup^.bRequest:= bRequest;
  setup^.wValue:= libusb_cpu_to_le16(wValue);
  setup^.wIndex:= libusb_cpu_to_le16(wIndex);
  setup^.wLength:= libusb_cpu_to_le16(wLength);
end;

{
  * \ingroup libusb_asyncio
  * Helper function to populate the required \ref libusb_transfer fields
  * for a control transfer.
  *
  * If you pass a transfer buffer to this function, the first 8 bytes will
  * be interpreted as a control setup packet, and the wLength field will be
  * used to automatically populate the \ref libusb_transfer::length "length"
  * field of the transfer. Therefore the recommended approach is:
  * -# Allocate a suitably sized data buffer (including space for control setup)
  * -# Call libusb_fill_control_setup()
  * -# If this is a host-to-device transfer with a data stage, put the data
  *    in place after the setup packet
  * -# Call this function
  * -# Call libusb_submit_transfer()
  *
  * It is also legal to pass a NULL buffer to this function, in which case this
  * function will not attempt to populate the length field. Remember that you
  * must then populate the buffer and length fields later.
  *
  * \param transfer the transfer to populate
  * \param dev_handle handle of the device that will handle the transfer
  * \param buffer data buffer. If provided, this function will interpret the
  * first 8 bytes as a setup packet and infer the transfer length from that.
  * This pointer must be aligned to at least 2 bytes boundary.
  * \param callback callback function to be invoked on transfer completion
  * \param user_data user data to pass to callback function
  * \param timeout timeout for the transfer in milliseconds
}

procedure libusb_fill_control_transfer(transfer: Plibusb_transfer; dev_handle: Plibusb_device_handle; buffer: PByte; callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
var
  setup: Plibusb_control_setup;
begin
  setup:= Plibusb_control_setup(buffer);
  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= 0;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_CONTROL);
  transfer^.timeout:= timeout;
  if setup <> nil then begin
    transfer^.length:= LIBUSB_CONTROL_SETUP_SIZE + libusb_le16_to_cpu(setup^.wLength);
  end;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end;


{
  * \ingroup libusb_asyncio
  * Helper function to populate the required \ref libusb_transfer fields
  * for a bulk transfer.
  *
  * \param transfer the transfer to populate
  * \param dev_handle handle of the device that will handle the transfer
  * \param endpoint address of the endpoint where this transfer will be sent
  * \param buffer data buffer
  * \param length length of data buffer
  * \param callback callback function to be invoked on transfer completion
  * \param user_data user data to pass to callback function
  * \param timeout timeout for the transfer in milliseconds
}
procedure libusb_fill_bulk_transfer(transfer: Plibusb_transfer; dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte; length: Integer; callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
begin
  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= endpoint;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_BULK);
  transfer^.timeout:= timeout;
  transfer^.buffer:= buffer;
  transfer^.length:= length;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end;


{
  * \ingroup libusb_asyncio
  * Helper function to populate the required \ref libusb_transfer fields
  * for a bulk transfer using bulk streams.
  *
  * Since version 1.0.19, \ref LIBUSB_API_VERSION >= 0x01000103
  *
  * \param transfer the transfer to populate
  * \param dev_handle handle of the device that will handle the transfer
  * \param endpoint address of the endpoint where this transfer will be sent
  * \param stream_id bulk stream id for this transfer
  * \param buffer data buffer
  * \param length length of data buffer
  * \param callback callback function to be invoked on transfer completion
  * \param user_data user data to pass to callback function
  * \param timeout timeout for the transfer in milliseconds
}

procedure libusb_fill_bulk_stream_transfer(transfer: Plibusb_transfer;
    dev_handle: Plibusb_device_handle; endpoint: Byte; stream_id: LongWord;
    buffer: PByte; length: Integer; callback: Tlibusb_transfer_cb_fn;
    user_data: Pointer; timeout: LongWord);
begin
  libusb_fill_bulk_transfer(transfer, dev_handle, endpoint, buffer, length,
      callback, user_data, timeout);
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_BULK_STREAM_30);
{ DONE : Später mit aufnehmen }
  libusb_transfer_set_stream_id(transfer, stream_id);
end;


{
  * \ingroup libusb_asyncio
  * Helper function to populate the required \ref libusb_transfer fields
  * for an interrupt transfer.
  *
  * \param transfer the transfer to populate
  * \param dev_handle handle of the device that will handle the transfer
  * \param endpoint address of the endpoint where this transfer will be sent
  * \param buffer data buffer
  * \param length length of data buffer
  * \param callback callback function to be invoked on transfer completion
  * \param user_data user data to pass to callback function
  * \param timeout timeout for the transfer in milliseconds
}

procedure libusb_fill_interrupt_transfer(transfer: Plibusb_transfer; dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte; length: Integer;	callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
begin
  transfer^.dev_handle:= dev_handle;
  transfer^.endpoint:= endpoint;
  transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_INTERRUPT);
  transfer^.timeout:= timeout;
  transfer^.buffer:= buffer;
  transfer^.length:= length;
  transfer^.user_data:= user_data;
  transfer^.callback:= callback;
end;


{
  * \ingroup libusb_asyncio
  * Helper function to populate the required \ref libusb_transfer fields
  * for an isochronous transfer.
  *
  * \param transfer the transfer to populate
  * \param dev_handle handle of the device that will handle the transfer
  * \param endpoint address of the endpoint where this transfer will be sent
  * \param buffer data buffer
  * \param length length of data buffer
  * \param num_iso_packets the number of isochronous packets
  * \param callback callback function to be invoked on transfer completion
  * \param user_data user data to pass to callback function
  * \param timeout timeout for the transfer in milliseconds
}

 procedure libusb_fill_iso_transfer(transfer: Plibusb_transfer; dev_handle: Plibusb_device_handle; endpoint: Byte; buffer: PByte; length: Integer; num_iso_packets: Integer; callback: Tlibusb_transfer_cb_fn; user_data: Pointer; timeout: LongWord);
 begin
   transfer^.dev_handle:= dev_handle;
   transfer^.endpoint:= endpoint;
   transfer^.type_:= Byte(LIBUSB_TRANSFER_TYPE_ISOCHRONOUS);
   transfer^.timeout:= timeout;
   transfer^.buffer:= buffer;
 	 transfer^.num_iso_packets:= num_iso_packets;
   transfer^.length:= length;
   transfer^.user_data:= user_data;
   transfer^.callback:= callback;
 end;

{
  * \ingroup libusb_asyncio
  * Convenience function to set the length of all packets in an isochronous
  * transfer, based on the num_iso_packets field in the transfer structure.
  *
  * \param transfer a transfer
  * \param length the length to set in each isochronous packet descriptor
  * \see libusb_get_max_packet_size()
}

procedure libusb_set_iso_packet_lengths(transfer: Plibusb_transfer; length: LongWord);
var
  i: Integer;
begin
  for i:= 0 to transfer^.num_iso_packets - 1 do begin
    transfer^.iso_packet_desc[i].length:= length;
  end;
end;


{
  * \ingroup libusb_asyncio
  * Convenience function to locate the position of an isochronous packet
  * within the buffer of an isochronous transfer.
  *
  * This is a thorough function which loops through all preceding packets,
  * accumulating their lengths to find the position of the specified packet.
  * Typically you will assign equal lengths to each packet in the transfer,
  * and hence the above method is sub-optimal. You may wish to use
  * libusb_get_iso_packet_buffer_simple() instead.
  *
  * \param transfer a transfer
  * \param packet the packet to return the address of
  * \returns the base address of the packet buffer inside the transfer buffer,
  * or NULL if the packet does not exist.
  * \see libusb_get_iso_packet_buffer_simple()
}


function libusb_get_iso_packet_buffer(transfer: Plibusb_transfer; packet: LongWord): PByte;
var
  i: Integer;
  offset: size_t;
  packet_: Integer;
begin
  offset:= 0;

{
  oops..slight bug in the API. packet is an unsigned int, but we use
  signed integers almost everywhere else. range-check and convert to
  signed to avoid compiler warnings. FIXME for libusb-2.
}

  Result:= nil;

  if packet > maxint then begin
    exit;
  end else begin
    packet_:= packet;
  end;

  if packet_ >= transfer^.num_iso_packets then begin
    exit;
  end;

  for i:= 0 to packet_ -1 do begin
    offset:= offset + transfer^.iso_packet_desc[i].length;
  end;

  Result:= transfer^.buffer + offset;
end;

{
  * \ingroup libusb_asyncio
  * Convenience function to locate the position of an isochronous packet
  * within the buffer of an isochronous transfer, for transfers where each
  * packet is of identical size.
  *
  * This function relies on the assumption that every packet within the transfer
  * is of identical size to the first packet. Calculating the location of
  * the packet buffer is then just a simple calculation:
  * <tt>buffer + (packet_size * packet)</tt>
  *
  * Do not use this function on transfers other than those that have identical
  * packet lengths for each packet.
  *
  * \param transfer a transfer
  * \param packet the packet to return the address of
  * \returns the base address of the packet buffer inside the transfer buffer,
  * or NULL if the packet does not exist.
  * \see libusb_get_iso_packet_buffer()
}

function libusb_get_iso_packet_buffer_simple(transfer: Plibusb_transfer; packet: LongWord): PByte;
var
  packet_: Integer;
begin

{
  oops..slight bug in the API. packet is an unsigned int, but we use
  signed integers almost everywhere else. range-check and convert to
  signed to avoid compiler warnings. FIXME for libusb-2.
}

  if packet > maxint then begin
    Result:= nil;
    exit;
  end;

  packet_:= Integer(packet);
  if packet_ >= transfer^.num_iso_packets then begin
    Result:= nil;
    exit;
  end;
  Result:= transfer^.buffer + NativeInt(transfer^.iso_packet_desc[0].length * packet_);
end;


{
  * \ingroup libusb_desc
  * Retrieve a descriptor from the default control pipe.
  * This is a convenience function which formulates the appropriate control
  * message to retrieve the descriptor.
  *
  * \param dev_handle a device handle
  * \param desc_type the descriptor type, see \ref libusb_descriptor_type
  * \param desc_index the index of the descriptor to retrieve
  * \param data output buffer for descriptor
  * \param length size of data buffer
  * \returns number of bytes returned in data, or LIBUSB_ERROR code on failure
}

function libusb_get_descriptor(dev_handle: Plibusb_device_handle;
    desc_type: Byte; desc_index: Byte; data: PByte; length: Integer): Integer;
begin
{ DONE : Später mit aufnehmen }
  Result:= libusb_control_transfer(dev_handle, Byte(LIBUSB_ENDPOINT_IN), LIBUSB_REQUEST_GET_DESCRIPTOR,
           Word(desc_type shl 8) or Word(desc_index), 0, data, Word(length), 1000);
end;


{
  * \ingroup libusb_desc
  * Retrieve a descriptor from a device.
  * This is a convenience function which formulates the appropriate control
  * message to retrieve the descriptor. The string returned is Unicode, as
  * detailed in the USB specifications.
  *
  * \param dev_handle a device handle
  * \param desc_index the index of the descriptor to retrieve
  * \param langid the language ID for the string descriptor
  * \param data output buffer for descriptor
  * \param length size of data buffer
  * \returns number of bytes returned in data, or LIBUSB_ERROR code on failure
  * \see libusb_get_string_descriptor_ascii()
}


function libusb_get_string_descriptor(dev_handle: Plibusb_device_handle;
    desc_index: Byte; langid: Word; data: PByte; length: Integer): Integer;
begin
{ DONE : Später mit aufnehmen }
  Result:= libusb_control_transfer(dev_handle, Byte(LIBUSB_ENDPOINT_IN),
               LIBUSB_REQUEST_GET_DESCRIPTOR,
               (Word(LIBUSB_DT_STRING) shl 8) or Word(desc_index),
               langid, data, Word(length), 1000);
end;

{$DEFINE DISABLE_LIBUSB1_POLL}
{$IFDEF ENABLE_LIBUSB1_POLL}
{
  * \ingroup libusb_poll
  * File descriptor for polling
  * Numeric file descriptor
  * Event flags to poll for from <poll.h>. POLLIN indicates that you
  * should monitor this file descriptor for becoming ready to read from,
  * and POLLOUT indicates that you should monitor this file descriptor for
  * nonblocking write readiness.
}
type
  Tlibusb_pollfd = record
    fd : longint;
    events : smallint;
  end;
{$PUSH}{$WARN 5028 OFF}
  Plibusb_pollfd = ^Tlibusb_pollfd;
{$POP}

{
  * \ingroup libusb_poll
  * Callback function, invoked when a new file descriptor should be added
  * to the set of file descriptors monitored for events.
  * \param fd the new file descriptor
  * \param events events to monitor for, see \ref libusb_pollfd for a
  * description
  * \param user_data User data pointer specified in
  * libusb_set_pollfd_notifiers() call
  * \see libusb_set_pollfd_notifiers()
}
  Tlibusb_pollfd_added_cb = procedure(fd: Integer; event: Word; user_data: Pointer); cdecl;


{
  * \ingroup libusb_poll
  * Callback function, invoked when a file descriptor should be removed from
  * the set of file descriptors being monitored for events. After returning
  * from this callback, do not use that file descriptor again.
  * \param fd the file descriptor to stop monitoring
  * \param user_data User data pointer specified in
  * libusb_set_pollfd_notifiers() call
  * \see libusb_set_pollfd_notifiers()
}
  Tlibusb_pollfd_removed_cb = procedure(fd: Integer; user_data: Pointer);
{$ENDIF}
end.
