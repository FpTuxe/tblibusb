program ShowMouseEvents;
{
  File: ShowMouseEvents.lpr
  CreateDate: 23.04.2021
  ModifyDate: 31.08.2024
  Version: 0.0.0.2
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  For more info to LibUsb see https://libusb.info/
}

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  {$DEFINE UseCThreads}
  {$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp, crt, fgl, tb_libusb1_0, tb_usb1_0_base;

const
  C_VERSION = '0.0.0.2';

type
  TVidPid = class
    VId: Word;
    PId: Word;
    IfNumber: Byte;
  end;

  { TMousesList }

  TMousesList = class(specialize TFPGObjectList<TVidPid>)
  public
    procedure Add(VId, PId: Word; aIfNumber: Byte);
  end;


  { TShowMouse }

  TShowMouse = class(TCustomApplication)
  private
    UsbIf: IbUsb1_0Base;
    MousesList: TMousesList;
    DefaultTab: Integer;
    VID: Word;
    PID: Word;
    OpenError: Boolean;
    ProgramExit: Boolean;

{$IFDEF LINUX}
    procedure DetachKernelDriver(DevHandle: Plibusb_device_handle;
      aInterfaceNumber: Byte);
{$ENDIF}
    function GetParam(const Token: String): String;
    function GetParamValue(const Token: String): Word;
    function GetVID: Word;
    function GetPID: Word;
    procedure Hello;
    function HexToWord(hex: String): Word;
    function IsMouse(const aDevice: Plibusb_device;
      const DeviceDescriptor: Tlibusb_device_descriptor;
      out aInterfaceNumber: Byte; out aEndPoint: Byte): Boolean;
    procedure ListMouses;
    procedure MakeUdev(Index: Integer);
    procedure MakeUdevReq;
    procedure ReadMouse(Index: Integer);
    procedure ReadMouse(aVID, aPID: Word; IfNumber: Byte=0);
    function ReadNumber: Integer;
    procedure ShowEvents;
    procedure ShowEventsOverVIdPid;
    procedure ShowMouseList;
    procedure ShowVersion;
    procedure WriteDriver;
    procedure WriteMouse(aNumber, aVId, aPid: Integer);
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TMousesList }

procedure TMousesList.Add(VId, PId: Word; aIfNumber: Byte);
var
  id: TVidPid;
begin
  id:= TVidPid.Create;
  id.VId:= VId;
  id.PId:= PId;
  id.IfNumber:= aIfNumber;
  inherited Add(id);
end;

{ TUdDescriptoren }

function TShowMouse.GetParam(const Token: String): String;
var
  i, p: Integer;
begin
  for i:= 1 to ParamCount do begin
    Result:= LowerCase(Params[i]);
    p:= Result.IndexOf(Token);
    if p >= 0 then begin
      Result:= Result.Substring(p+Token.Length);
      exit;
    end;
  end;
  Result:= '';
end;

function TShowMouse.HexToWord(hex: String): Word;
var
  p, c: Integer;
begin
  p:= hex.IndexOf('0x');
  if p >= 0 then begin
    Delete(hex, 1, p+2);
    hex:= '$' + hex;
  end;

  Val(hex, Result, c);
  if c <> 0 then begin
    Result:= 0;
  end;
end;

function TShowMouse.GetParamValue(const Token: String): Word;
var
  t: String;
begin
  t:= GetParam(Token);
  if t = '' then begin
    Result:= 0;
    exit;
  end;
  Result:= HexToWord(t)
end;

function TShowMouse.GetVID: Word;
begin
  Result:= GetParamValue('vid=');
end;

function TShowMouse.GetPID: Word;
begin
  Result:= GetParamValue('pid=');
end;

function TShowMouse.IsMouse(const aDevice: Plibusb_device;
  const DeviceDescriptor: Tlibusb_device_descriptor; out
  aInterfaceNumber: Byte; out aEndPoint: Byte): Boolean;
var
  config_desc: Plibusb_config_descriptor;
  interface_desc: Plibusb_interface_descriptor;
  c, i: Integer;
begin
  for c:= 0 to DeviceDescriptor.bNumConfigurations -1 do begin
    UsbIf.GetConfigDescriptor(aDevice, c, config_desc);
    for i:= 0 to config_desc^.bNumInterfaces -1 do begin
      UsbIf.GetInterfaceDescriptorByIndex(config_desc, interface_desc, i);
      if (interface_desc^.bInterfaceClass = Ord(LIBUSB_CLASS_HID))
      and (interface_desc^.bInterfaceProtocol = $02) {Mouse} then begin
        aInterfaceNumber:= interface_desc^.bInterfaceNumber;
        aEndPoint:= interface_desc^.endpoint[0].bEndpointAddress;
        Result:= True;
        exit;
      end;
    end;
  end;
  Result:= False;
end;

{$IFDEF LINUX}
procedure TShowMouse.DetachKernelDriver(DevHandle: Plibusb_device_handle;
    aInterfaceNumber: Byte);
begin
  try
    UsbIf.DetachKernelDriver(DevHandle, aInterfaceNumber);
  except
    on EbUsb1_0 do WriteLn('Is detached!');
  end;
end;
{$ENDIF}

procedure TShowMouse.WriteMouse(aNumber, aVId, aPid: Integer);
begin
  WriteLn(Format('%.02u, Mouse, VID=0x%.04X, PID=0x%.04X', [aNumber, aVId, aPid]));
end;

procedure TShowMouse.ListMouses;
var
  interface_nb: Byte = 0;
  end_point: Byte = 0;
  c: Integer = 0;
  v, p: Word;
  device_list: PPlibusb_device;
  amount_devices, i: Integer;
  device: Plibusb_device;
  dev_handle: Plibusb_device_handle = Nil;
  dev_desc: Tlibusb_device_descriptor;

begin
  MousesList.Clear;
  {$PUSH}{$WARN 5057 OFF}
  amount_devices:= UsbIf.GetDeviceList(device_list);
  {$POP}
  for i:= 0 to amount_devices -1 do try
    device:= UsbIf.GetDevice(i, device_list);
    UsbIf.OpenDevice(device, dev_handle);
    UsbIf.GetDeviceDescriptor(device, dev_desc);
    if IsMouse(device, dev_desc, interface_nb, end_point) then begin
      inc(c);
      v:= dev_desc.idVendor;
      p:= dev_desc.idProduct;
      WriteMouse(c, v, p);
      MousesList.Add(v, p, interface_nb);
    end;
    UsbIf.CloseDevice(dev_handle);
  except
    on E: ELibUsb do begin
      OpenError:= True;
      WriteLn('Exception!! Message=', E.Message);
      UsbIf.CloseDevice(dev_handle);
    end;
  end;
end;

function TShowMouse.ReadNumber: Integer;
var
  readed: String;
begin
  ReadLn(readed);
  Result:= StrToIntDef(readed, 0);
end;

procedure TShowMouse.ShowMouseList;
var
  i: Integer;
begin
  for i:= 0 to MousesList.Count - 1 do begin
    WriteMouse(i+1, MousesList[i].VId, MousesList[i].PId);
  end;
end;

procedure TShowMouse.ShowVersion;
var
  version: Tlibusb_version;
begin
  UsbIf.GetVersion(version);
  WriteLn('ShowMouseEvents, version=', C_VERSION);
  WriteLn('LibUsb version=', UsbIf.VersionToString(version));
  WriteLn('LibUsb header version=', UsbIf.GetHeaderVersion);
  WriteLn;
end;

procedure TShowMouse.ReadMouse(Index: Integer);
begin
  with MousesList[Index] do begin
    ReadMouse(VId, PId, IfNumber);
  end;
end;

procedure TShowMouse.ReadMouse(aVID, aPID: Word; IfNumber: Byte = 0);
var
  dev_handle: Plibusb_device_handle = Nil;
  device: Plibusb_device;
  device_desc: Tlibusb_device_descriptor;
  confg_desc: Plibusb_config_descriptor;
  if_desc: Plibusb_interface_descriptor;
  ep_desc: Plibusb_endpoint_descriptor;
  buffer: array of Byte;
  received, b: Integer;
  e: char = '-';

begin
  WriteLn('ReadMouse');
  WriteLn('Exit with enter 9');
  try
    try
      UsbIf.OpenDevice(aVID, aPID, dev_handle);
      device:= UsbIf.GetDevice(dev_handle);
      UsbIf.GetDeviceDescriptor(device, device_desc);
      UsbIf.GetConfigDescriptor(device, 0, confg_desc);
      UsbIf.GetInterfaceDescriptorByIndex(confg_desc, if_desc, IfNumber);
{$IFDEF LINUX}
      DetachKernelDriver(dev_handle, if_desc^.bInterfaceNumber);
{$ENDIF}
      UsbIf.ClaimInterface(dev_handle, if_desc^.bInterfaceNumber);
      ep_desc:= @if_desc^.endpoint[0];
{$PUSH}{$WARN 5091 OFF}
      SetLength(buffer, ep_desc^.wMaxPacketSize);
{$POP}
      repeat
        UsbIf.InterruptTransfer(dev_handle, ep_desc^.bEndpointAddress,
            @buffer[0], ep_desc^.wMaxPacketSize, received, 1000, true);
        if received > 0 then begin;
          for b:= 0 to received -1 do begin
            Write(IntToHex(buffer[b]), ' ');
          end;
          WriteLn;
        end;
        if KeyPressed then begin
          e:= ReadKey;
        end;
      until e = '9';
    except
      on E:EbUsb1_0 do begin
        ProgramExit:= True;
        WriteLn('Exception=', E.Message);
      end;
    end;
  finally
{$IFDEF LINUX}
     UsbIf.SetAutoDetachKernelDriver(dev_handle, True);
{$ENDIF}
     UsbIf.ReleaseInterface(dev_handle, if_desc^.bInterfaceNumber);
     UsbIf.CloseDevice(dev_handle);
  end;
end;

procedure TShowMouse.ShowEvents;
var
  m: Integer;
begin
  repeat
    WriteLn;
    if MousesList.Count = 0 then begin
      WriteLn('First enter 1,  List mouses');
      exit;
    end;
    ShowMouseList;
    WriteLn('Enter a Number to select a mouse and turn enter');
    WriteLn('9 = Exit');
    m:= ReadNumber;
    if (m > 0) and (m <= MousesList.Count) then begin
      ReadMouse(m-1);
      Continue;
    end;
  until (m=9) or ProgramExit;
end;

procedure TShowMouse.ShowEventsOverVIdPid;
var
  v_id, p_id: Word;
  readed: String;
begin
  WriteLn('Enter IDs in hex format 2 bytes, example "0x046d"');
  repeat
    WriteLn('Enter VId');
    ReadLn(readed);
    v_id:= HexToWord(readed);
  until v_id <> 0;
  repeat
    WriteLn('Enter PId');
    ReadLn(readed);
    p_id:= HexToWord(readed);
  until p_id <> 0;
  ReadMouse(v_id, p_id);
end;

procedure TShowMouse.Hello;
begin
  repeat
    WriteLn;
    WriteLn('Hello');
    WriteLn('What are you want to do?');
    WriteLn('Hit a number and return');
{$IFDEF UNIX}
    WriteLn('1   List mouses, needed root rights');
    WriteLn('2   Make a UDev rule, needed root rights');
    WriteLn('3   Show mouses events over list, needed root rights');
{$ENDIF}
{$IFDEF WINDOWS}
    WriteLn('1   List mouses. Needed root rights');
    WriteLn('2   Make Windows Driver');
    WriteLn('3   Show mouses events');
{$ENDIF}
    WriteLn('4   Show mouses events over VID, PID');
    WriteLn('5   Help');
    WriteLn('9   Exit');
    case ReadNumber of
      1: ListMouses;
{$IFDEF UNIX}
      2: MakeUdevReq;
{$ENDIF}
{$IFDEF WINDOWS}
      2: WriteDriver;
{$ENDIF}
      3: ShowEvents;
      4: ShowEventsOverVIdPid;
      5: WriteHelp;
      9: ProgramExit:= True;
    end;
  until ProgramExit;
end;

procedure TShowMouse.MakeUdev(Index: Integer);
const

  FMT_UDEV = '#Attention! Write Vendor and Product ID only in lowercase letters.' + sLineBreak
           + 'SUBSYSTEM=="usb", ATTRS{idVendor}=="%s", ATTRS{idProduct}=="%s", MODE="0660", OWNER="%s", RUN+="/usr/bin/logger -s ''ShowMouseEvent''"';

var
  aOwner: String;
  rule, fn: String;
  sl: TStringList;
  aVId, aPId: Word;

  function Hex(aId: Word): String;
  begin
    Result:= Format('%.04X', [aId]).ToLower;
  end;

begin
  WriteLn('Enter Owner (User).');
  Readln(aOwner);
  aVId:= MousesList[Index].VId;
  aPid:= MousesList[Index].PId;
  rule:= Format(FMT_UDEV, [Hex(aVId), Hex(aPid), aOwner]);
  fn:= Format('99-ShowMouseEvents-%s-%s.rules', [Hex(aVId), Hex(aPId)]);
  try
    sl:= TStringList.Create;
    sl.Add(rule);
    sl.SaveToFile(fn);
  finally
    FreeAndNil(sl);
  end;

  WriteLn;
  WriteLn('Ready!!! Follow the descripton');
  WriteLn;
  WriteLn('Disconnect mouse');
  WriteLn('UDev rule saved to "', fn, '"');
  WriteLn('copy as root to /lib/udev/rules.d/');
  WriteLn('  sudo cp ', fn, ' /lib/udev/rules.d/');
  WriteLn('Reload the rules');
  WriteLn('  sudo udevadm control --reload-rules');
  WriteLn('  sometimes you need a reboot');
  WriteLn('Connect the NEW mouse');
  WriteLn('Start the program without options and user right');
  WriteLn('  Select 3 show mouses events');
  WriteLn('    Select the NEW mouse');
  WriteLn('Thats all');
  WriteLn;
  ProgramExit:= True;
end;

procedure TShowMouse.MakeUdevReq;
var
  i: Integer;
begin
  WriteLn;
  ShowMouseList;

  if MousesList.Count = 0 then begin
    WriteLn('Sorry no mouse found');
    WriteLn('First enter 1,  List mouses');
    exit;
  end;

  repeat
    WriteLn('Enter a number to select the mouse, 9 = exit');
    i:= ReadNumber;
    if (i > 0) and (i <= MousesList.Count) then begin
      MakeUdev(i-1);
      exit;
    end;
  until i = 9;
end;

procedure TShowMouse.DoRun;
var
  ErrorMsg: String;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('h l v', 'help');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  VID:= GetVID;
  PID:= GetPID;

  if HasOption('l') then begin
    ListMouses;
    Terminate;
    Exit;
  end;

  if HasOption('v') then begin
    ShowVersion;
    Terminate;
    Exit;
  end;

  if (VID<>0) and (PID<>0) then begin
    ReadMouse(VID, PID);
  end else begin
    ShowVersion;
    Hello;
  end;

  { add your program here }

{$IFDEF UNIX}
  if OpenError and not ProgramExit then begin
    WriteLn('Run with root rights, to show all!')
  end;
{$ENDIF}
  // stop program loop
  WriteLn();
  Terminate;
end;

constructor TShowMouse.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
  UsbIf:= TIbUsb1_0Base.Create; //UsbIf is an Interface and destroyed automaticle
  MousesList:= TMousesList.Create(True);
  DefaultTab:= 2;
end;

destructor TShowMouse.Destroy;
begin
  FreeAndNil(MousesList);
  inherited Destroy;
end;

procedure TShowMouse.WriteDriver;
begin
  {$IFDEF UNIX}
    WriteLn;
    WriteLn('To do!');
    WriteLn(' Connect a second mouse');
    WriteLn(' To create a udev rule, start the program with root rights and');
    WriteLn(' no options');
    WriteLn(' Select 2, make a UDev rule');
    WriteLn('   Select the NEW mouse device');
    WriteLn('   Enter owner (user name)');
    WriteLn(' Disconnect the mouse');
    WriteLn(' Follow the descriptons');
  {$ENDIF}
  {$IFDEF WINDOWS}
    WriteLn;
    WriteLn('To do!');
    WriteLn(' Connect a second mouse');
    WriteLn(' To create a driver, start Zadig');
    WriteLn('  Options->List ALL Devices checked');
    WriteLn('  Select the NEW mouse device');
    WriteLn('  Click replace driver');
    WriteLn('  Follow the descriptons');
  {$ENDIF}
end;

procedure TShowMouse.WriteHelp;
begin
  { add your help code here }
  WriteLn('ShowMouseEvents, version = ', C_VERSION);
  WriteLn('  This programm required a driver');
  WriteDriver;
  WriteLn;
  WriteLn('Options');
  WriteLn('  -l  List all mouses');
  WriteLn('  -v  Version');
  WriteLn('With no options, the dialog was started');
  WriteLn;
  WriteLn('Read a mouse');
  WriteLn('  Example: ShowMouseEvents vid=0x046d pid=0xc050');
  WriteLn;
end;

var
  Application: TShowMouse;

{$R *.res}

begin
  Application:=TShowMouse.Create(nil);
  Application.Title:='ShowMouseEvents';
  Application.Run;
  Application.Free;
end.

