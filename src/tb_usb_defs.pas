unit tb_usb_defs;

{
  File: tb_usb_defs.pas
  CreateDate: 01.03.2019
  ModifyDate: 28.09.2023
  Version: 0.2.0.0
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
}

{$mode objfpc}{$H+}

interface

{$DEFINE OBJECT_STAT_DISABLED}  
{$IFDEF OBJECT_STAT_ENABLED}
uses
  Classes,
  SysUtils,
  tb_objects;

type
  TUsbObject = class(TBObject);
  TUsbInterfacedObject = class(TBInterfacedObject);
{$ELSE}
uses
  Classes,
  SysUtils;

type
  TUsbObject = class(TObject);
  TUsbInterfacedObject = class(TInterfacedObject);
{$ENDIF}

implementation

end.

