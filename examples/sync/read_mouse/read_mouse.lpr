program read_mouse;
{
  File: read_mouse.lpr
  CreateDate: 20.02.2024
  ModifyDate: 31.08.2024
  Version: 0.0.0.3
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  For more info to LibUsb see https://libusb.info/
}
{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Classes,
  sysutils,
  crt,
  tb_libusb1_0,
  tb_usb1_0_base
  { you can add units after this };

const
{$HINT Warning!!! Please use not the main mouse, connect a other mouse to use}
  YOUR_DECIVE_VID = $046d; {$HINT Here set your mouse VID}
  YOUR_DECIVE_PID = $c077; {$HINT Here set your mouse PID}


procedure ReadMouse(VId, PId: Word);
var
  dev_handle: Plibusb_device_handle = Nil;
  device: Plibusb_device;
  device_desc: Tlibusb_device_descriptor;
  confg_desc: Plibusb_config_descriptor;
  if_desc: Plibusb_interface_descriptor;
  ep_desc: Plibusb_endpoint_descriptor;
  buffer: array of Byte;
  received, b: Integer;
  e: char = '-';
  UsbIf: IbUsb1_0Base;

begin
  WriteLn('The VId and PId is hard coded!');
  WriteLn('ReadMouse VId=$', IntToHex(VId), ', PId=$', IntToHex(PId));
  WriteLn('Exit with enter q');
  try
    try
      UsbIf:= TIbUsb1_0Base.Create;
      UsbIf.OpenDevice(VId, PId, dev_handle);
      device:= UsbIf.GetDevice(dev_handle);
      UsbIf.GetDeviceDescriptor(device, device_desc);
      UsbIf.GetConfigDescriptor(device, 0, confg_desc);
      UsbIf.GetInterfaceDescriptorByIndex(confg_desc, if_desc, 0);
{$IFDEF LINUX}
      try
        UsbIf.DetachKernelDriver(dev_handle, if_desc^.bInterfaceNumber);
      except
        on EbUsb1_0 do WriteLn('Mouse is detached');
      end;
{$ENDIF}
      UsbIf.ClaimInterface(dev_handle, if_desc^.bInterfaceNumber);
      ep_desc:= @if_desc^.endpoint[0];
{$PUSH}{$WARN 5091 OFF}
      SetLength(buffer, ep_desc^.wMaxPacketSize);
{$POP}
      repeat
        UsbIf.InterruptTransfer(dev_handle, ep_desc^.bEndpointAddress,
            @buffer[0], ep_desc^.wMaxPacketSize, received, 1000, true);
        if received > 0 then begin
          for b:= 0 to received -1 do begin
            Write(IntToHex(buffer[b]), ' ');
          end;
          WriteLn;
        end;
        if KeyPressed then begin
          e:= ReadKey;
        end;
      until e = 'q';
    except
      on E:EbUsb1_0 do begin
        WriteLn('Exception=', E.Message);
      end;
    end;
  finally
 {$IFDEF LINUX}
    UsbIf.SetAutoDetachKernelDriver(dev_handle, True);
 {$ENDIF}
  end;
  UsbIf.ReleaseInterface(dev_handle, if_desc^.bInterfaceNumber);
  UsbIf.CloseDevice(dev_handle);
  WriteLn('Byte');
end;

begin
  ReadMouse(YOUR_DECIVE_VID, YOUR_DECIVE_PID)
end.

