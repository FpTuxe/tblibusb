{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit tblibusb;

{$warn 5023 off : no warning about unused units}
interface

uses
  tb_libusb1_0, tb_libusb1_0_thread, tb_usb1_0_base, tb_usb_defs, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('tblibusb', @Register);
end.
