unit tb_libusb1_0_thread;

{
  File: tb_libusb1_0_thread.pas
  CreateDate: 31.05.2019
  ModifyDate: 31.05.2019
  Version: 2.0.0.2
  Author: Thomas Bulla

  See the file COPYING.TXT, included in this distribution,
  for details about the copyright.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
}

{$mode objfpc}{$H+}

 //Unter Linux
 // als erstes in der Program Pas folgendes in die uses Klause schreiben.
 // uses
 // {$IFDEF UNIX}{$IFDEF UseCThreads}
 // cthreads,
 // cmem,
 // {$ENDIF}{$ENDIF}
 // ...


{ TbUsb1_0Thread:
  TThread hat ein unsch�nes verhalten beim Beenden eines Threads.
  Will man Waitfor verwenden um das beenden es Threads abzuwarten,
  wird man entt�uscht. Die Funktion Waitfor kehrt dann zur�ck,
  wenn Terminate aufgerufen wurde welche wiederum Terminated�
  auf True setzt. Damit wird zwar dem Thread signalisiert, dass er sich
  beenden soll, hat er aber noch nicht getan. Wenn man jetzt weiter aufr�umt
  und irgendwelche Objekte l�scht, die der noch laufende Thread ben�tigt,
  dann kracht es. Diese Problematik l�st FinThread.
  Wichtig ist, dass in dem Thread Loop nicht die Variable Terminated�
  abgefragt wird, sondern Finalized, siehe Beispiel.

  Die Reihenfolge beim Beenden sieht wie folgt aus:
  Finalize; -> es wird die Variable Finalized von False auf True besetzt.
  Thread l�uft und beendet sich, und setzt als letztes Terminated auf True.


  Die State Reihenfolge vom Starten bis zum Beenden des Threads:
  fts_stop:			    Thread l�uft noch nicht;
  fts_start:			  Wird gestartet
  fts_started:		  Ist gestartet
  fts_finalize: 	  Wird Beendet
  fts_terminated:   Ist Beendet. Ab hier kann aufger�umt werden

Beispiel:

TMyThread = class(TbUsb1_0Thread)
protected
  Procedure Run; override;
  Destructor Destroy; override;
end;


TMyThread.Run;
begin
  while not Finalized begin
    Hier den Quellcode platzieren
  end;
end;

TMyThread.Destroy;
begin
  Finalize;	  //Das Beenden des Threads einleiten
  WaitFor;    //Warten bis der Thread beendet ist
  Und weiter aufr�umen
  inherited;
End;

Wenn es was zum Aufr�umen gibt, z.B. Klassen die im Thread erzeugt wurden,
dann am besten in der Methode Clenup implementieren.
Diese Methode wird am Ende der Thread-Lebenszeit aufgerufen.

TMyThread.Cleanup; override;

TMyThread.Cleanup;
begin
  Was so weg muss.
end;

}

{ TB_Runnable:
TB_Runnable erzeugt einen Thread, der nach Beendigung freigegeben wird.
Beim Erzeugen des Threads wird ein Interface oder eine Procedure mit �bergeben,
die dann ausgef�hrt wird.

Beispiel A:

type
TTest_Runnable = class(TInterfacedObject, IB_Runnable)
public
  procedure Run(Sender: TB_Runnable);
end;

Implementation

procedure TTest_Runnable.Run(Sender: TB_Runnable);
begin
  while not Sender.Finished do begin
//Irgendwas
  end;
end;

//Erzeugen des Threads
TB_Runnable.Create(TTest_Runnable.Create);

================================================================================

Beispiel B:

procedure RunProc(const Sender: TB_Runnable; Parameter: TObject);
begin
  while not Sender.Finished do begin
//Irgendwas
  end;
end;

//Erzeugen des Threads
TB_Runnable.Create(RunProc);

================================================================================

procedure FreeAndNilThread(var Thread);

Ist das Ponton zu FreeAndNil, nur wir hier gepr�ft, ob der Thread sich selbst
zum L�schen aufruft. Wenn JA, dann wird nicht Destroy aufgerufen, sondern die
Eigenschaft FreeOnTerminate auf True gesetzt, was daf�r sorgt, dass der Thread
sich beendet.
}


interface

uses
  Classes,
  sysutils,
  syncobjs;

type
  Tbusb1_0ThreadState = (fts_stop, fts_stoped, fts_start, fts_started,
                     fts_finalize, fts_terminated);

  { TbUsb1_0Thread }

  TbUsb1_0Thread = class(TThread)
  private
    FFinalized: Boolean;
    FStopEvent: TEvent;
    function GetFinalized: Boolean;
    function GetStoped: Boolean;
  protected
    FState: Tbusb1_0ThreadState;
    FThreadHalt: TEvent;
    procedure Execute; override; final;
    procedure Run; virtual; abstract;
    procedure OnCleanup; virtual;
    procedure Finalize;
    property State: Tbusb1_0ThreadState read FState;
    property Finalized: Boolean read GetFinalized;
    property Stoped: Boolean read GetStoped;
  public
    constructor Create(CreateSuspended: Boolean;
                       const StackSize: SizeUInt = DefaultStackSize);
    destructor Destroy; override;
    procedure Start;
    function Stop(const TimeoutMs: Integer): Boolean;
  end;

  TbUsb1_0Runnable = class;

  IbUsb1_0Runnable = Interface
    procedure Run(Sender: TbUsb1_0Runnable);
  end;

  TbUsb1_0RunnableProc = procedure(const Sender: TbUsb1_0Runnable; Parameter: TObject);

  { TbUsb1_0Runnable }

  TbUsb1_0Runnable = class(TbUsb1_0Thread)
  private
    FRun: IbUsb1_0Runnable;
    FRunProc : TbUsb1_0RunnableProc;
    FRunParameter: TObject;
  protected
    procedure Run; override;
  public
    constructor Create(const RunIf: IbUsb1_0Runnable;
                       const CreatePriority: TThreadPriority = tpNormal;
                       const CreateSuspended: Boolean = False;
                       const StackSize: SizeUInt = DefaultStackSize); overload;

    constructor Create(const RunProc: TbUsb1_0RunnableProc;
                       const Parameter: TObject = nil;
                       const CreatePriority: TThreadPriority = tpNormal;
                       const CreateSuspended: Boolean = False;
                       const StackSize: SizeUInt = DefaultStackSize); overload;
  end;


  procedure FreeAndNilThread(var Thread);

implementation

{$ifdef TB_OBJECTSTAT_ENABLED}
uses
  tb_objects_stat;
{$endif}

procedure FreeAndNilThread(var Thread);
var
  CID: TThreadId;
  finthread: TbUsb1_0Thread;
begin
  finthread:= TbUsb1_0Thread(Thread);

  if Assigned(finthread) then begin
    CID:= GetCurrentThreadId;
    if CID = finthread.ThreadID then begin
      finthread.FreeOnTerminate:= True;
      finthread.Finalize;
      pointer(Thread):=nil;
    end else begin
      FreeAndNil(Thread);
    end;
  end;
end;

{ TbUsb1_0Runnable }

procedure TbUsb1_0Runnable.Run;
begin
  if Assigned(FRun) then begin
    FRun.Run(Self);
  end else if Assigned(FRunProc) then begin
    FRunProc(Self, FRunParameter);
  end
end;

constructor TbUsb1_0Runnable.Create(const RunIf: IbUsb1_0Runnable;
                               const CreatePriority: TThreadPriority;
                               const CreateSuspended: Boolean;
                               const StackSize: SizeUInt);
begin
  inherited Create(True, StackSize);
  FRun:= RunIf;
  self.Priority:= CreatePriority;
  self.FreeOnTerminate:= True;
  if not CreateSuspended then begin
     Start;
  end;
end;

constructor TbUsb1_0Runnable.Create(const RunProc: TbUsb1_0RunnableProc;
                               const Parameter: TObject;
                               const CreatePriority: TThreadPriority;
                               const CreateSuspended: Boolean;
                               const StackSize: SizeUInt);
begin
  inherited Create(True, StackSize);
  FRunProc:= RunProc;
  FRunParameter:= Parameter;
  self.Priority:= CreatePriority;
  self.FreeOnTerminate:= True;
  if not CreateSuspended then begin
     Start;
  end;
end;


{ TbUsb1_0Thread }

function TbUsb1_0Thread.GetFinalized: Boolean;
begin
  if FFinalized then begin
    FStopEvent.SetEvent;
  end else if FState = fts_stop then begin
    FState:= fts_stoped;
    FStopEvent.SetEvent;
    FThreadHalt.WaitFor($FFFFFFFF);
    FThreadHalt.ResetEvent;
    FState:= fts_start;
    FState:= fts_started;
  end;
  result:= FFinalized;
end;

function TbUsb1_0Thread.GetStoped: Boolean;
begin                //Geaendert + Suspended
  result:= (fts_stoped = FState) or Suspended;
end;

procedure TbUsb1_0Thread.Execute;
begin
  FState:= fts_start;
  FState:= fts_started;
  Run;
  Terminate;
  FState:= fts_terminated;
  OnCleanUp;
end;

procedure TbUsb1_0Thread.OnCleanup;
begin
  FreeAndNil(FStopEvent);
  FreeAndNil(FThreadHalt);
{$ifdef TB_OBJECTSTAT_ENABLED}
  TObjectStat.This.Release(self);
//  __RmInstance(self.ClassType);
{$endif}
end;

procedure TbUsb1_0Thread.Finalize;
begin
  if Assigned(FThreadHalt) then begin;
    FFinalized:= True;
    FThreadHalt.SetEvent;
    FState:= fts_finalize;
    if Suspended then begin
      Start;
    end;
  end;
end;

constructor TbUsb1_0Thread.Create(CreateSuspended: Boolean;
  const StackSize: SizeUInt);
begin
  inherited;
  FThreadHalt:= TEvent.Create(nil, True, False, 'FinThread halt event');
  FStopEvent:= TEvent.Create(nil, True, False, 'FinThread stop event');
{$ifdef TB_OBJECTSTAT_ENABLED}
  TObjectStat.This.Add(self);
//  __AddInstance(self.ClassType);
{$endif}
end;

destructor TbUsb1_0Thread.Destroy;
begin
// See OnCleanup
//  FreeAndNil(FStopEvent);
//  FreeAndNil(FThreadHalt);
//{$ifdef TB_OBJECTSTAT_ENABLED}
//  __RmInstance(self.ClassType);
//{$endif}
  inherited Destroy;
end;

procedure TbUsb1_0Thread.Start;
begin
  if FState = fts_stoped then begin
    FThreadHalt.SetEvent;
  end else begin
    inherited Start;
  end;
end;

function TbUsb1_0Thread.Stop(const TimeoutMs: Integer): Boolean;
var
  r: TWaitResult;
begin
  FState:= fts_stop;
  r:= FStopEvent.WaitFor(TimeoutMs);
  FStopEvent.ResetEvent;
  if r = wrSignaled then begin
    result:= True;
  end else begin  //Falls der Thread sp�ter gestartet wurde
    if FState <> fts_stop then begin
      FState:= fts_stop;
      r:= FStopEvent.WaitFor(TimeoutMs);
      FStopEvent.ResetEvent;
    end;
    result:= r = wrSignaled;
  end;
end;

end.
